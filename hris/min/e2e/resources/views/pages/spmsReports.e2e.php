<?php
   $user = "";
   $incRptScrn = getvalue("hRptScrn");
?>

<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script language="JavaScript" src="<?php echo jsCtrl("ctrl_spmsReport"); ?>"></script>
   </head>
   <body>
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"spms"); ?>
         <div class="container-fluid" id="mainScreen">
            <div class="row panel-top padd5">
               <a href="javascript:void(0)" class="mbar" id="titleBarIcons" onclick="openNav();">
                  <i class="fa fa-chevron-left" aria-hidden="true"></i>
               </a>&nbsp;
               <?php
                  echo strtoupper(getvalue("paramTitle"));
                  if (getvalue("hRptScrn") != "") {
               ?>
               <button type="button" class="close" aria-label="Close" onclick="closeSCRN('model');">
                  <span aria-hidden="true" style="color:white;">&times;</span>
               </button>
               <?php } ?>
            </div>
            <?php spacer(10);?>
            <div class="container-fluid margin-top">
               <div class="row">
                  <div class="col-xs-12" id="div_CONTENT">
                  <?php if ($incRptScrn == "") { ?>
                     <div class="mypanel" id="ReportHead">
                        <div class="panel-top">Report Type</div>
                        <div class="panel-mid-litebg">
                           <div class="row margin-top">
                              <div class="col-xs-3">
                                 <div class="InfoMenu" id="Report1">
                                    <img src="<?php echo img("applicants.png"); ?>">&nbsp;Report 1
                                 </div>
                                 <div class="InfoMenu" id="Report2">
                                    <img src="<?php echo img("employees.png"); ?>">&nbsp;Report 2
                                 </div>
                                 <div class="InfoMenu" id="Report3">
                                    <img src="<?php echo img("baby.png"); ?>">&nbsp;Report 3
                                 </div>
                                 <div class="InfoMenu" id="Report4">
                                    <img src="<?php echo img("educ.png"); ?>">&nbsp;Report 4
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  <?php } else { ?>
                     <div class="row">
                        <div class="col-xs-12" id="RptMainHolder">
                           <?php include $incRptScrn.".e2e.php"; ?>
                        </div>
                     </div>
                  <?php } ?>
                  </div>
               </div>
            </div>
            <?php
               footer();
               include_once ("varHidden.e2e.php");
            ?>
         </div>
         <input type="hidden" id="hRptFile" value="<?php echo getvalue("hRptFile")?>">
      </form>
   </body>
</html>