<?php
   //session_start();
   include 'colors.e2e.php';
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";

   $rs = SelectEach("employees","LIMIT ".getvalue("count"));
   if ($rs) $rowcount = mysqli_num_rows($rs);
   switch (getvalue("rptIDX")) {
      case 1:
         $rptTitle = "TOTAL EMPLOYEES";
      break;
      case 2:
         $rptTitle = "NEWLY REGULAR";
      break;
      case 3:
         $rptTitle = "END OF CONTRACT";
      break;
      case 4:
         $rptTitle = "RESIGNED / TERMINATED";
      break;
      case 5:
         $rptTitle = "NOTIFICATION FOR BIRTHDAY";
      break;
      case 6:
         $rptTitle = "NOTIFICATION FOR ANNIVERSARY";
      break;
      case 7:
         $rptTitle = "NOTIFICATION FOR PROMOTED";
      break;
      case 8:
         $rptTitle = "NOTIFICATION FOR RETIREES";
      break;
      case 9:
         $rptTitle = "STEP INCREMENT AND LOYALTY (3 YEARS)";
      break;
      case 10:
         $rptTitle = "STEP INCREMENT AND LOYALTY (4 - 10 YEARS)";
      break;
      case 11:
         $rptTitle = "STEP INCREMENT AND LOYALTY (11 - 15 YEARS)";
      break;
      case 12:
         $rptTitle = "STEP INCREMENT AND LOYALTY (16 - UP YEARS)";
      break;
      case 13:
         $rptTitle = "NO BIRTH CERTIFICATE";
      break;
      case 14:
         $rptTitle = "NO TRANSCRIPT OF RECORDS (TOR)";
      break;
      case 15:
         $rptTitle = "DUAL CITIZEN";
      break;
      case 16:
         $rptTitle = "EMP. w/ GRAD. STUDIES";
      break;
   }
?>

<!DOCTYPE html>
<html>
   <head>
      <?php include "pageHEAD.e2e.php"; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <style>
         td {vertical-align:top;}
      </style>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            rptHeader($rptTitle);
            if ($rs)
            {

         ?>
            <table border="1">
               <tr>
                  <th>#</th>
                  <th>REFID</th>
                  <th>LAST NAME</th>
                  <th>FIRST NAME</th>
                  <th>MIDDLE NAME</th>
                  <th>DEPT.</th>
                  <th>POSITION</th>
                  <th>HIRED DATE</th>
                  <th>START DATE</th>
               </tr>

               <?php
                  $count = 0;
                  while ($rowEMP = mysqli_fetch_assoc($rs)) {
                     $count++;
                     $rowEMPINFO = FindFirst("empinformation","WHERE EmployeesRefId = ".$rowEMP["RefId"],"*");

                     $dept = "";
                     $position = "";
                     $hireDate = "";
                     $startDate = "";
                     $dept = getRecord("Department",$rowEMPINFO["DepartmentRefId"],"Name");
                     $position = getRecord("Position",$rowEMPINFO["PositionRefId"],"Name");
                     $hireDate = $rowEMPINFO["HiredDate"];
                     $startDate = $rowEMPINFO["StartDate"];

                     echo
                     '<tr>
                        <td class="txt-center" style="width:50px;background:#ededed;">'.$count.'</td>
                        <td class="txt-center">'.$rowEMP["RefId"].'</td>
                        <td>'.$rowEMP["LastName"].'</td>
                        <td>'.$rowEMP["FirstName"].'</td>
                        <td>'.$rowEMP["MiddleName"].'</td>
                        <td class="txt-center">'.$dept.'</td>
                        <td class="txt-center">'.$position.'</td>
                        <td class="txt-center">'.$hireDate.'</td>
                        <td class="txt-center">'.$startDate.'</td>
                     </tr>';
                  }
               ?>
            </table>
            <?php
               rptFooter();
            }
            ?>
      </div>
   </body>
</html>