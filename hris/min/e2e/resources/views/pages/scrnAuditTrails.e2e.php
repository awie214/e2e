<?php
   include 'colors.e2e.php';
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once "conn.e2e.php";

   if (empty($p_TrnFrom)) {
      $date_today = date("Y-m-d",time());
      $date = date_create($date_today);
      //date_sub($date,date_interval_create_from_date_string("1 day"));
      $p_TrnFrom = date_format($date,"Y-m-d");
   }
   if (empty($p_TrnTo)) {
      $date_today = date("Y-m-d",time());
      $date = date_create($date_today);
      //date_sub($date,date_interval_create_from_date_string("1 day"));
      $p_TrnTo = date_format($date,"Y-m-d");
   }

?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
      <script type="text/javascript">
         $(document).ready(function () {
            $("[name='txtTrnTo']").change(function () {
               var from = $("[name='txtTrnFrom']").val().replace(/-/g,'/');
               var to = $("[name='txtTrnTo']").val().replace(/-/g,'/');
               if(Date.parse(from) > Date.parse(to)){
                   alert("Invalid Date Range " + from + " To " + to);
                   $("#btnGENERATE").prop("disabled",true);
                   clearFields('txtTrnFrom,txtTrnTo');
                   $("[name='txtTrnFrom']").select();
                   $("[name='txtTrnFrom']").focus();
                   return false;
               }else {
                  $("#btnGENERATE").prop("disabled",false);
               }
            });
         });
      </script>
   </head>
   <body>
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"pis"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar("Audit Trail"); ?>
            <input type="hidden" value="<?php echo getvalue("paramTitle"); ?>" name="paramTitle">
            <div class="container-fluid margin-top" id="rptCriteria">
               <?php
                  $attr = ["empRefId"=>getvalue("txtRefId"),
                           "empLName"=>getvalue("txtLName"),
                           "empFName"=>getvalue("txtFName"),
                           "empMName"=>getvalue("txtMidName")];
                  $empRefId = EmployeesSearch($attr);
               ?>
               <div class="row">
                  <div class="col-xs-12" id="div_CONTENT">
                     <div class="row margin-top">
                        <div class="col-xs-2 txt-right">
                           <span class="label">Transaction Date:</span>
                        </div>
                        <div class="col-xs-4">
                           <span class="label">From:</span>
                           <input type="text" class="form-input date-- rptCriteria-- wid30" style="width:30%;" name="txtTrnFrom" placeholder="Lower Date" value="<?php echo $p_TrnFrom ?>">
                           <span class="label">To:</span>
                           <input type="text" class="form-input date-- rptCriteria-- wid30" style="width:30%;" name="txtTrnTo" placeholder="Higher Date" value="<?php echo $p_TrnTo ?>">
                           <a href="javascript:clearFields('txtTrnFrom,txtTrnTo');" class="clearflds--">&nbsp;&nbsp;Clear</a>
                        </div>
                        <button type="button"
                                value="Generate"
                                class="btn-cls4-sea trnbtn"
                                name="btnGENERATE"
                                id="btnGENERATE">
                           <i class="fa fa-list-alt" aria-hidden="true"></i>&nbsp;
                           Generate
                        </button>
                     </div>
                  </div>
               </div>
            </div>
            <?php
               footer();
               include "varHidden.e2e.php";
            ?>
         </div>
         <!-- Modal -->
         <input type="hidden" id="ReportKind" value="rptAuditTrails">
         <div class="modal fade border0" id="prnModal" role="dialog">
            <div class="modal-dialog border0" style="padding:0px;width:97%;height:92%;">
               <div class="mypanel border0" style="height:100%;">
                  <div class="panel-top bgSilver">
                     <a href="#" data-toggle="tooltip" data-placement="top" title="Print Now" id="btnPRINTNOW">
                        <i class="fa fa-print" aria-hidden="true"></i>
                     </a>
                     <button type="button" class="close" data-dismiss="modal">&times;</button>
                  </div>
                  <iframe id="rptContent" src="blank.e2e.php" class="iframes"></iframe>
               </div>
            </div>
         </div>
      </form>
   </body>
</html>



