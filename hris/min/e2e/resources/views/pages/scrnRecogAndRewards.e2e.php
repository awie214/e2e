<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script language="JavaScript" src="<?php echo jsCtrl("ctrl_RecogAndReward"); ?>"></script>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"pis"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar($modTitle); ?>
            <div class="container-fluid margin-top10">
               <div class="row">
                  <div class="col-xs-3">
                     <?php
                        employeeSelector();
                     ?>
                  </div>
                  <div class="col-xs-9">
                     <div id="divList">
                        <div class="panel-top">
                           LIST OF AWARD of<br>
                           <span id="selectedEmployees">&nbsp;</span>
                        </div>
                        <div class="panel-mid-litebg">
                           <div class="row">
                              <div class="col-xs-12">
                                 <div style="overflow:auto;max-height:500px;">
                                    <div id="spGridTable">
                                       <?php
                                          $table = "employeesrecognitionandreward";
                                          $gridTableHdr = "Employee ID|Award|Date";
                                          $gridTableFld = "AgencyId|AwardsRefId|YearReceived";
                                          $gridTableHdr_arr = explode("|",$gridTableHdr);
                                          $gridTableFld_arr = explode("|",$gridTableFld);
                                          $sql = "SELECT * FROM `$table` WHERE RefId = 0 ORDER BY RefId Desc LIMIT 100";
                                          $_SESSION["module_table"] = $table;
                                          $_SESSION["module_gridTableHdr_arr"] = $gridTableHdr_arr;
                                          $_SESSION["module_gridTableFld_arr"] = $gridTableFld_arr;
                                          $_SESSION["module_sql"] = $sql;
                                          $_SESSION["module_gridTable_ID"] = "gridTable";
                                          doGridTable($table,
                                                      $gridTableHdr_arr,
                                                      $gridTableFld_arr,
                                                      $sql,
                                                      [true,true,true,false],
                                                      $_SESSION["module_gridTable_ID"]);
                                       ?>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="panel-bottom">
                           <?php btnINRECLO(['true','true','true']);?>
                        </div>
                     </div>
                     <div class="row" id="divView">
                        <div class="col-xs-8" style="padding-left:15px;" id="EntryScrn">
                           <input type="hidden" class="form-input saveFields--" name="sint_EmployeesRefId">
                           <div class="row margin-top">
                              <div class="col-xs-3">
                                 <label>Employee ID:</label>
                              </div>
                              <div class="col-xs-6">
                                 <input type="text" class="form-input saveFields--" name="sint_EmployeesId" disabled>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-3">
                                 <label>Award:</label>
                              </div>
                              <div class="col-xs-6">
                                 <?php
                                    $table = "awards";
                                    createSelect($table,
                                                 "sint_AwardsRefId",
                                                 "",100,"Name","Select Award","");

                                 ?>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-3">
                                 <label>Date:</label>
                              </div>
                              <div class="col-xs-3">
                                 <input type="text" class="form-input date-- saveFields--" name="date_YearReceived">
                              </div>
                           </div>
                           <div class="row margin-top" id="idSAVECANCEL">
                              <div class="col-xs-12">
                                 <?php 
                                    spacer(10);
                                    btnSACABA(['true','true','true']);
                                 ?>
                              </div>
                           </div>
                        </div>

                     </div>
                  </div>
               </div>
            </div>

            <?php
               footer();
               doHidden("EmpRefIdRequire","YES","");
               $table = "employeesrecognitionandreward";
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>

   </body>
</html>