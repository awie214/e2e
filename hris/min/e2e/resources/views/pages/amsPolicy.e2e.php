<?php
   $tabTitle = ["Leave Policy Group","Leave Policy","Overtime Policy Group","Overtime Policy"];
   $tabFile = ["inc_ams_LeavePolicyGroup","inc_ams_LeavePolicy","inc_ams_OvertimePolicyGroup","inc_ams_OvertimePolicy"];
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script type="text/javascript" src="<?php echo jsCtrl("ctrl_amsPolicy"); ?>"></script>
      <script type="text/javascript">
         $(document).ready(function () {
            <?php
               if (isset($_GET["Tab"])) {
                  echo 'tabClick('.$_GET["Tab"].');';
               } else {
                  echo 'tabClick(1);';
               }
               if (isset($_GET["hTable"])) {
                  echo '$("#hTable").val("'.$_GET["hTable"].'");';
               }
               if (isset($_GET["hModalName"])) {
                  echo '$("#hModalName").val("'.$_GET["hModalName"].'");';
               }
               if (isset($_GET["hEntryScreen"])) {
                  echo '$("#hEntryScreen").val("'.$_GET["hEntryScreen"].'");';
               }
               
            ?>
         });
      </script>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" id="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"ams"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar ($paramTitle); ?>
            <div class="container-fluid margin-top">
               <div class="row">
                  <div class="col-xs-12" id="div_CONTENT">
                     <div class="row">
                        <?php //createButton("ATTACHMENT","btnATTACH","btn-cls4-lemon trnbtn","fa-paperclip","btnATTACH"); ?>
                     </div>
                     <div class="row" style="margin-top:5px;width:100%;">
                           <div class="col-xs-12">
                              <input type="hidden" name="hTabIdx" value="1">
                              <div class="btn-group btn-group-sm">
                                 <?php
                                    $idx = 0;
                                    $active = "";
                                    for ($j=0;$j<count($tabTitle);$j++) {
                                       $idx = $j + 1;
                                       /*if ($idx == 1) $active = "elActive";
                                                 else $active = "";*/
                                       echo
                                       '<button type="button" name="btnTab_'.$idx.'" class="btn btn-default '.$active.'">'.$tabTitle[$j].'</button>';
                                    }
                                 ?>
                              </div>
                              <div class="row">
                                 <div class="col-xs-12">
                                    <?php
                                       spacer(10);
                                       $idx = 0;
                                       for ($j=0;$j<count($tabTitle);$j++) {
                                          $idx = $j + 1;
                                          fTabs($idx,$tabTitle[$j],$tabFile[$j]);
                                       }
                                    ?>
                                 </div>
                              </div>
                           </div>
                     </div>
                  </div>
               </div>
            </div>
            <?php
               footer();
               doHidden("fn","SaveRecord","");
               doHidden("hEntryScreen","","");
               doHidden("hModalName","","");
               include "varHidden.e2e.php";
            ?>
         </div>
         <!-- Modal -->
         <div class="modal fade modalFieldEntry--" id="modalFieldEntry" role="dialog">
            <div class="modal-dialog">
               <div class="mypanel" style="height:100%;">
                  <div class="panel-top bgSea">
                     <span id="modalTitle" style="font-size:11pt;">Inserting New Policy Group - </span>
                     <span id="PolicyType" style="font-size:11pt;"></span>
                     <button type="button" class="close" data-dismiss="modal">&times;</button>
                  </div>
                  <div class="panel-mid" id="EntryScrn">
                     <div class="row margin-top">
                        <div class="col-xs-4 txt-right">
                           <?php label("Code:",""); ?>
                        </div>
                        <div class="col-xs-8">
                           <?php createForm("text","char_Code","Code","","","saveFields--") ?>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-4 txt-right">
                           <?php label("Name:",""); ?>
                        </div>
                        <div class="col-xs-8">
                           <?php createForm("text","char_Name","Name","","","saveFields--") ?>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-4 txt-right">
                           <?php label("Description:",""); ?>
                        </div>
                        <div class="col-xs-8">
                           <?php createForm("text","char_Description","Description","","","saveFields--") ?>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-4 txt-right">
                           <?php label("Remarks:",""); ?>
                        </div>
                        <div class="col-xs-8">
                           <textarea class="form-input saveFields--" rows="5" name="char_Remarks" placeholder="remarks"></textarea>
                        </div>
                     </div>
                  </div>
                  <div class="panel-bottom">
                     <div class="row">
                        <div class="col-xs-12 txt-center">
                           <?php createButton("Save","btnLocSave","btn-cls4-sea","fa-floppy-o",""); ?>
                           <?php createButton("Cancel","btnLocCancel","btn-cls4-red","fa-undo",""); ?>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </form>
   </body>
</html>