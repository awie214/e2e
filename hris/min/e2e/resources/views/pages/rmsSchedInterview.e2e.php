<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script language="JavaScript" src="<?php echo jsCtrl("ctrl_AfterTrn"); ?>"></script>
   </head>
   <body>
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"rms"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar(getvalue("paramTitle")); ?>
            <div class="container-fluid margin-top10">
               <div class="row">
                  <div class="col-xs-12" id="divList">
                     <div class = "mypanel">
                        <div class = "panel-top">LIST</div>
                        <div class = "panel-mid">
                           <div class="row">
                              <div class="col-xs-12">
                                 <div id="spGridTable">
                                    <?php
                                          $sql = "SELECT * FROM `".strtolower($table)."` ORDER BY RefId Desc LIMIT 500";
                                          doGridTable($table,
                                                      $gridTableHdr_arr,
                                                      $gridTableFld_arr,
                                                      $sql,
                                                      [true,true,true],
                                                      $_SESSION["module_gridTable_ID"]);
                                    ?>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="panel-bottom">
                           <?php
                              btnINRECLO([true,true,false]);
                           ?>
                        </div>
                     </div>
                  </div>
                  <div id="divView">
                     <div class="mypanel">
                        <div class="panel-top">
                           INSERTING NEW SCHEDULE FOR INTERVIEW
                        </div>
                        <div class="panel-mid">
                           <div id="EntryScrn">
                              <div class="row" id="badgeRefId">
                                 <div class="col-xs-5">
                                    <ul class="nav nav-pills">
                                       <li class="active" style="font-size:12pt;font-weight:600;">
                                          <a>REFID : <span class="badge" style="font-size:12pt;font-weight:600;" id="idRefid">
                                          </span></a>
                                       </li>
                                    </ul>
                                 </div>
                              </div>
                              <?php
                                 spacer(15);
                                 $attr = array(
                                    "row"=>true,
                                    "col"=>"6",
                                    "label"=>"Position Applied",
                                    "table"=>"PositionItem"
                                 );
                                 createFormFK($attr);
                                 $list = [
                                    array("row"=>true,
                                          "name"=>"date_DateApplied",
                                          "col"=>"6",
                                          "id"=>"DateApplied",
                                          "label"=>"Date Applied",
                                          "class"=>"date-- mandatory",
                                          "style"=>"width:50%;"),

                                    array("row"=>true,
                                          "name"=>"date_InitialDateInterview",
                                          "col"=>"6",
                                          "id"=>"InitialDateInterview",
                                          "label"=>"Initial Date Interview",
                                          "class"=>"date-- mandatory",
                                          "style"=>"width:50%;"),
                                    array("row"=>true,
                                          "name"=>"char_InitialInterviewByRefId",
                                          "col"=>"6",
                                          "id"=>"InitialInterviewedBy",
                                          "label"=>"Initial Interviewed By",
                                          "class"=>"mandatory",
                                          "style"=>""),
                                    array("row"=>true,
                                          "name"=>"char_InitialInterviewStatus",
                                          "col"=>"6",
                                          "id"=>"InitInterviewStatus",
                                          "label"=>"Initial Interview Status",
                                          "class"=>"mandatory",
                                          "style"=>""),

                                    array("row"=>true,
                                          "name"=>"date_FinalDateInterview",
                                          "col"=>"6",
                                          "id"=>"FinalDateInterview",
                                          "label"=>"Final Date Interview",
                                          "class"=>"date-- mandatory",
                                          "style"=>"width:50%;"),
                                    array("row"=>true,
                                          "name"=>"char_FinalInterviewByRefId",
                                          "col"=>"6",
                                          "id"=>"FinalInterviewedBy",
                                          "label"=>"Final Interviewed By",
                                          "class"=>"mandatory",
                                          "style"=>""),
                                    array("row"=>true,
                                          "name"=>"char_FinalInterviewStatus",
                                          "col"=>"6",
                                          "id"=>"FinalInterviewStatus",
                                          "label"=>"Final Interview Status",
                                          "class"=>"mandatory",
                                          "style"=>""),

                                    array("row"=>true,
                                          "name"=>"char_FinalStatus",
                                          "col"=>"6",
                                          "id"=>"FinalStatus",
                                          "label"=>"Final Status",
                                          "class"=>"mandatory",
                                          "style"=>"")
                                 ];
                                 createInput($list);
                              ?>
                              <div class="row">
                                 <div class="col-xs-6">
                                    <div class="form-group">
                                       <label class="control-label" for="inputs">Remarks:</label>
                                       <textarea class="form-input saveFields--" rows="5" name="char_Remarks" placeholder="remarks"></textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <?php
                              btnSACABA([true,true,true]);
                           ?>
                        </div>
                        <div class="panel-bottom"></div>
                     </div>
                  </div>
               </div>
            </div>
            <?php
               footer();
               include_once ("varHidden.e2e.php");
            ?>
         </div>
      </form>
   </body>
</html>




