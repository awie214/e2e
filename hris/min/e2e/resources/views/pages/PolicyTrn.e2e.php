<?php
	error_reporting(E_ALL);
   ini_set('display_errors', 1);
   session_start();
   include_once "conn.e2e.php";
   include_once "constant.e2e.php";
   include_once pathClass.'0620functions.e2e.php';
   include_once pathClass.'SysFunctions.e2e.php';
   include_once pathClass.'0620TrnData.e2e.php';

   $moduleContent = file_get_contents(json.'policy.json');
   $module        = json_decode($moduleContent, true); 

   function fnSaveRecord() {
      $conn = $GLOBALS["conn"];
      $module = $GLOBALS["module"];
      $empRefid = getvalue("emprefid");
      $table = getvalue("hTable");
      $elem1 = "TabName";
      $dbField = $module[$elem1][$table]["dbField"];
      $tab = $module[$elem1][$table]["TabCount"];
      $arr = parseFieldnValue($dbField,$_POST);
      $Fldnval = $arr["FieldsValues"];
      $Fields = $arr["Fields"];
   	$Values = $arr["Values"];
      $params = "&Tab=".$tab;
      $params .= "&hTable=".$table;
      $params .= "&hModalName=".getvalue("hModalName");
      $params .= "&hEntryScreen=".getvalue("hEntryScreen");
   	if (getvalue("hRefId") > 0) {
   		$update = f_SaveRecord("EDITSAVE",$table,$Fldnval,getvalue("hRefId"));
   		if ($update != "") {
   			echo $update;
   		} else {
   			echo 'alert("Record Successfully Updated.");';
   			echo 'gotoscrn("amsPolicy","'.$params.'");';
   		}
   	} else {
   		$SaveSuccessful = f_SaveRecord("NEWSAVE",$table,$Fields,$Values);
   		if (!is_numeric($SaveSuccessful)) {
   			echo $SaveSuccessful;
   		} else {
   			echo 'alert("Record Successfully Save.");';
   			echo 'gotoscrn("amsPolicy","'.$params.'");';
   		}
   	}
   }
   
   
   /*DONT MODIFY HERE*/
   $funcname = "fn".getvalue("fn");
   $params   = getvalue("params");
   if (!empty($funcname)) {
      $funcname($params);
   } else {
      echo 'alert("Error... No Function defined");';
   }
?>