<!DOCTYPE html>
<html>
<body>

<?php
   require "constant.e2e.php";
   require "conn.e2e.php";
   require_once pathClass."0620functions.e2e.php";

   $f = "1Province.csv";
   $myfile = fopen($f, "r") or die("Unable to open file!");

            $conn->query("truncate table `city`");
            $conn->query("truncate table `province`");
            $loop = 0;
            $inserted = 0;
            while(!feof($myfile)) {
               $loop++;
               if ($loop != 1) {

                  $t = time();
                  $date_today    = date("Y-m-d",$t);
                  $curr_time     = date("H:i:s",$t);
                  $trackingA_fld = "`LastUpdateDate`, `LastUpdateTime`, `LastUpdateBy`, `Data`";
                  $trackingA_val = "'$date_today', '$curr_time', 'PHP', 'M'";

                  $str = fgets($myfile);
                  $str_Arr = explode(",",$str);
                  $CityName = remquote($str_Arr[0]);
                  $Class = $str_Arr[1];
                  $ProvinceName = remquote($str_Arr[2]);
                  $ProvinceRefId = fileManager($ProvinceName,
                                            "province",
                                            "`Code`,`Name`,`Remarks`,",
                                            "'','".$ProvinceName."','',");

                  $flds = "`ProvinceRefId`, `Name`, `Class`, `Remarks`,".$trackingA_fld;
                  $values = "'$ProvinceRefId','$CityName','$Class','',".$trackingA_val;
                  $sql = "INSERT INTO `city` ($flds) VALUES ($values)";
                  if ($conn->query($sql)) {
                     $inserted++;
                  } else {
                     echo $sql;
                  }
               }
            }
            echo "Inserted Records : $inserted";
   fclose($myfile);
?>

</body>
</html>