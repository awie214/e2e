<?php 
   $module = module("ReqChangeShift"); 
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script language="JavaScript" src="<?php echo jsCtrl("ctrl_AfterTrn"); ?>"></script>
      <script type="text/javascript">
         function afterDelete() {
            alert("Successfully Deleted");
            gotoscrn($("#hProg").val(),"");
         }
      </script>
   </head>
   <body>
      <form name="xForm" method="post" action="postMultiple.e2e.php">
         <?php $sys->SysHdr($sys,"ams"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar($paramTitle); ?>
            <div class="container-fluid margin-top">
               <div class="row">
                  <div class="col-xs-12" id="div_CONTENT">
                        <div id="divList">
                           <div class="mypanel">
                              <div class="panel-top">LIST</div>
                              <div class="panel-mid">
                                 <span id="spGridTable">
                                    <?php
                                       $sizeCol = "col-sm-6";
                                       if ($UserCode == "COMPEMP") {
                                          $sizeCol = "col-sm-12";
                                          $gridTableHdr_arr = ["File Date", "Start Date", "End Date", "Status"];
                                          $gridTableFld_arr = ["FiledDate", "StartDate", "EndDate", "Status"];
                                          $sql = "SELECT * FROM $table WHERE CompanyRefId = $CompanyId AND BranchRefId = $BranchId AND EmployeesRefId = $EmployeesRefId ORDER BY FiledDate";
                                          $Action = [false,false,false,false];
                                       }
                                       doGridTable($table,
                                                   $gridTableHdr_arr,
                                                   $gridTableFld_arr,
                                                   $sql,
                                                   $Action,
                                                   "gridTable");
                                    ?>
                                 </span>
                                 <?php
                                       btnINRECLO([true,true,false]);
                                 ?>
                              </div>
                           </div>
                        </div>
                        <div id="divView">
                           <div class="row">
                              <div class="col-xs-6">
                                       <div class="mypanel">
                                          <div class="panel-top">
                                             <span id="ScreenMode">REQUESTING </span> <?php echo strtoupper($ScreenName); ?>
                                          </div>
                                          <div class="panel-mid">
                                             <div id="EntryScrn">
                                                <div class="row margin-top">
                                                   <div class="<?php echo $sizeCol; ?>">
                                                      <div class="row" id="badgeRefId">
                                                         <div class="col-xs-6">
                                                            <ul class="nav nav-pills">
                                                               <li class="active" style="font-size:12pt;font-weight:600;">
                                                                  <a>REFID : <span class="badge" style="font-size:12pt;font-weight:600;" id="idRefid">
                                                                  </span></a>
                                                               </li>
                                                            </ul>
                                                         </div>
                                                      </div>
                                                      <?php
                                                         $attr = ["br"=>true,
                                                                  "type"=>"text",
                                                                  "row"=>true,
                                                                  "name"=>"date_FiledDate",
                                                                  "col"=>"4",
                                                                  "id"=>"FiledDate",
                                                                  "label"=>"Date File",
                                                                  "class"=>"saveFields-- mandatory date--",
                                                                  "style"=>"",
                                                                  "other"=>""];
                                                         $form->eform($attr);

                                                         echo
                                                         '<div class="row margin-top">
                                                            <div class="col-xs-12">';
                                                            $attr = array(
                                                               "row"=>true,
                                                               "col"=>"12",
                                                               "label"=>"Work Schedule",
                                                               "table"=>"WorkSchedule"
                                                            );
                                                            createFormFK($attr);
                                                         echo
                                                         '  </div>
                                                         </div>';

                                                         echo
                                                         '<div class="row margin-top">
                                                            <div class="col-xs-12">
                                                               <div class="row">';
                                                                  $attr = ["br"=>true,
                                                                           "type"=>"text",
                                                                           "row"=>false,
                                                                           "name"=>"date_StartDate",
                                                                           "col"=>"6",
                                                                           "id"=>"StartDate",
                                                                           "label"=>"Start Date",
                                                                           "class"=>"saveFields-- mandatory date--",
                                                                           "style"=>"",
                                                                           "other"=>""];
                                                                  $form->eform($attr);
                                                                  $attr = ["br"=>true,
                                                                           "type"=>"text",
                                                                           "row"=>false,
                                                                           "name"=>"date_EndDate",
                                                                           "col"=>"6",
                                                                           "id"=>"EndDate",
                                                                           "label"=>"End Date",
                                                                           "class"=>"saveFields-- mandatory date--",
                                                                           "style"=>"",
                                                                           "other"=>""];
                                                                  $form->eform($attr);
                                                         echo
                                                         '
                                                               </div>
                                                            </div>
                                                         </div>';
                                                      ?>
                                                      <div class="row margin-top">
                                                         <div class="col-xs-12">
                                                            <div class="form-group">
                                                               <label class="control-label" for="inputs">Remarks:</label>
                                                               <textarea class="form-input saveFields--" rows="5" name="char_Remarks" placeholder="remarks"></textarea>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="col-xs-6 padd5 adminUse--">
                                                      <label>Employees Selected</label>
                                                      <select multiple id="EmpSelected" name="EmpSelected" class="form-input" style="height:250px;">
                                                      </select>
                                                      <p>Double Click the items to remove in the List</p>
                                                      <input type="hidden" class="saveFields--" name="hEmpSelected">
                                                   </div>
                                                </div>
                                             </div>
                                             <?php
                                                spacer(10);
                                                echo
                                                '<button type="submit" class="btn-cls4-sea"
                                                         name="btnLocSAVE" id="LocSAVE">
                                                   <i class="fa fa-floppy-o" aria-hidden="true"></i>
                                                   &nbsp;Save
                                                </button>';
                                                btnSACABA([false,true,true]);
                                             ?>
                                          </div>
                                       </div>
                              </div>
                              <div class="col-xs-6 adminUse--">
                                 <div class="mypanel">
                                    <div class="panel-top">Employees List</div>
                                    <div class="panel-mid">
                                       <?php include_once "incEmpListSearchCriteria.e2e.php" ?>
                                       <!-- <a href="javascript:void(0);" title="Search Employees">
                                          <i class="fa fa-search" aria-hidden="true"></i>
                                       </a>-->
                                       <div id="empList" style="max-height:300px;overflow:auto;">
                                          <h4>No Employees Selected</h4>
                                       </div>
                                    </div>
                                    <div class="panel-bottom">
                                       <a href="javascript:void(0);" id="clearEmpSelected">Clear Employees Selected</a>&nbsp;
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                  </div>
               </div>
            </div>
            <?php
               footer();
               include "varJSON.e2e.php";
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>

   </body>
</html>