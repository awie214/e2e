<?php require_once "incUtilitiesJS.e2e.php"; ?>
   <div class="mypanel" id="rptCriteria">
      <div class="panel-top">
         Options
      </div>
      <div class="panel-mid-litebg">
         <div class="row margin-top">
            <div class="col-xs-12 txt-center">
               <span class="label">Sort By:</span>
               <select class="form-input rptCriteria--" name="drpSortBy" id="drpSortBy" style="width:150px;">
                  <option value="LastName" selected>Last Name</option>
                  <option value="FirstName">First Name</option>
                  <option value="BirthDate">Date of Birth</option>
                  <option value="Height">Height</option>
                  <option value="Weight">Weight</option>
                  <option value="CivilStatus">Civil Status</option>
               </select>
            </div>
         </div>
      </div>
      <div class="panel-top">
         Show Column
      </div>
      <div class="panel-mid-litebg padd10">
         <div class="row">
            <div class="col-xs-3">
               <input type="checkbox" id="checkAll"/>&nbsp;<span class="label">Check All</span>
               <?php bar();?>
            </div>
         </div>
         <?php
            $ObjList =
            [
               [
                  "chkEmpRefId|EmpRefId|checked|Employees Ref. Id",
                  "chkEmpName|EmpName|checked disabled|Employees Name",
                  "chkPosition|Position|checked|Position",
                  "chkSG|SalGrade|checked|Salary Grade",
               ],
               [
                  "chkSS|SalStep|checked|Salary Step",
                  "chkEducation|Education|checked|Education",
                  "chkTraining|Training|checked|Training"
               ]
            ];

            for ($j=0;$j<count($ObjList);$j++) {
               $RowElement = $ObjList[$j];
               echo
               '<div class="row margin-top">';
               for ($d=0;$d<count($RowElement);$d++) {
                  $obj = explode("|",$RowElement[$d]);

                  if ($obj[0] != "") {
                     echo
                     '<div class="col-xs-3 colopt">
                        <input type="checkbox" id="'.$obj[1].'" name="'.$obj[0].'" class="showCol--" '.$obj[2].'>
                        <label class="label" for="'.$obj[1].'">'.$obj[3].'</label>
                     </div>';
                  }

               }
               echo
               '</div>'."\n";
            }
         ?>
      </div>
   </div>
