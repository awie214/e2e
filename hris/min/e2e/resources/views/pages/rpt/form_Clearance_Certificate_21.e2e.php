<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $table = "employees";
   $whereClause .= " ORDER BY LastName LIMIT 10";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) {
      echo $whereClause;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
      <style type="text/css">
         .border-right-bottom-left {
            border-right: 2px solid black;
            border-bottom: 2px solid black;
            border-left: 2px solid black;
         }
      </style>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            $count = 0;
            if ($rsEmployees) {
               while ($row_emp = mysqli_fetch_assoc($rsEmployees)) {
                  $FullName   = $row_emp["LastName"].", ".$row_emp["FirstName"]." ".$row_emp["MiddleName"];
                  $emp_info   = FindFirst("empinformation","WHERE EmployeesRefId = ".$row_emp["RefId"],"*");
                  /*if ($emp_info) {*/
                     $date_hired = $emp_info["HiredDate"];
                     if ($date_hired != "") {
                        $date_hired = date("m/d/Y",strtotime($date_hired));
                     } else {
                        $date_hired = "(NO HIRED DATE)";
                     }
                     $position   = rptDefaultValue($emp_info["PositionRefId"],"position");
                     $division   = rptDefaultValue($emp_info["DivisionRefId"],"division");
         ?>
         <div class="row" style="page-break-after: always;">
            <div class="col-xs-12">
               <div class="row">
                  <div class="col-xs-12">
                     <?php
                        rptHeader(getvalue("RptName"));
                     ?>
                  </div>
               </div>
               <div class="row margin-top border-right-bottom-left" style="border-top: 2px solid black;">
                  <div class="col-xs-12">
                     <div class="row" style="border-bottom: 1px solid black;">
                        <div class="col-xs-6">
                           Name: <?php echo strtoupper($FullName); ?>
                        </div>
                        <div class="col-xs-6" style="border-left: 1px solid black;">
                           Position: <?php echo $position; ?>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xs-6">
                           Section/Division: <?php echo $division; ?>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row border-right-bottom-left">
                  <div class="col-xs-12">
                     <div class="row margin-top">
                        <div class="col-xs-12">
                           <b>PURPOSE OF CLEARANCE</b> <i>(Pls. Check)</i>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-3">
                           <input type="checkbox" disabled>&nbsp;Sick Leave
                        </div>
                        <div class="col-xs-3">
                           <input type="checkbox" disabled>&nbsp;Maternity Leave
                        </div>
                        <div class="col-xs-3">
                           <input type="checkbox" disabled>&nbsp;Retirement Leave
                        </div>
                        <div class="col-xs-3">
                           <input type="checkbox" disabled>&nbsp;Others, pls. specify
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-3">
                           <input type="checkbox" disabled>&nbsp;Vacation Leave
                        </div>
                        <div class="col-xs-3">
                           <input type="checkbox" disabled>&nbsp;Study Leave
                        </div>
                        <div class="col-xs-3">
                           <input type="checkbox" disabled>&nbsp;Resignation
                        </div>
                        <div class="col-xs-3">
                           ________________________
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-12">
                           <b>Period Covered/Effective Date:</b>
                           ________________________________________________________________________
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row border-right-bottom-left">
                  <div class="col-xs-12">
                     <div class="row margin-top">
                        <div class="col-xs-12 text-center">
                           <b>INSTRUCTIONS</b>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-12">
                           1. This form shall be filed in triplicate and shall be sent by the concerned employee to the different sections/divisions for signature.
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-12">
                           2.  Submit the completed clearance to the Human Resources and Records Management Section (HRRMS).
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-12">
                           3. No last salary or any monetary claims shall be paid to employees on leave without pay or separated from the government service unless clearance is approved and submitted to the HRRMS.
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row border-right-bottom-left">
                  <div class="col-xs-12">
                     <div class="row margin-top">
                        <div class="col-xs-12 text-center">
                           <b>CERTIFICATION</b>
                           <br>
                        </div>
                     </div>
                     <div class="row margin-top" style="padding: 5px;">
                        <div class="col-xs-6">
                           <div class="row margin-top">
                              <div class="col-xs-12"><b>1. Work Accountabilities</b></div>
                           </div>
                           <div class="row margin-top text-center">
                              <div class="col-xs-12" style="border-bottom: 1px solid black; width:95%;">
                                 &nbsp;
                              </div>
                           </div>
                           <div class="row text-center">
                              <div class="col-xs-12">
                                 Division Chief
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-12"><b>3. Cooperative Obligations</b></div>
                           </div>
                           <div class="row margin-top text-center">
                              <div class="col-xs-12" style="border-bottom: 1px solid black; width:95%;">
                                 &nbsp;
                              </div>
                           </div>
                           <div class="row text-center">
                              <div class="col-xs-12">
                                 President, MEMABAI
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-12"><b>5. Money Accountabilities</b></div>
                           </div>
                           <div class="row margin-top text-center">
                              <div class="col-xs-12" style="border-bottom: 1px solid black; width:95%;">
                                 &nbsp;
                              </div>
                           </div>
                           <div class="row text-center">
                              <div class="col-xs-12">
                                 Cashier, Administrative & General Services Section
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-12"><b>7. Property Accountabilities</b></div>
                           </div>
                           <div class="row margin-top text-center">
                              <div class="col-xs-12" style="border-bottom: 1px solid black; width:95%;">
                                 &nbsp;
                              </div>
                           </div>
                           <div class="row text-center">
                              <div class="col-xs-12">
                                 Administrative Officer V  
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-12"><b>9. Provident Fund</b></div>
                           </div>
                           <div class="row margin-top text-center">
                              <div class="col-xs-12" style="border-bottom: 1px solid black; width:95%;">
                                 &nbsp;
                              </div>
                           </div>
                           <div class="row text-center">
                              <div class="col-xs-12">
                                 Chairman  
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-12"><b>RECOMMENDING APPROVAL:</b></div>
                           </div>
                           <div class="row margin-top text-center">
                              <div class="col-xs-12" style="border-bottom: 1px solid black; width:95%;">
                                 &nbsp;
                              </div>
                           </div>
                           <div class="row text-center">
                              <div class="col-xs-12">
                                 Chief, Finance and Administrative Division
                              </div>
                           </div>
                        </div>
                        <div class="col-xs-6">
                           <div class="row margin-top">
                              <div class="col-xs-12"><b>2.  Library Obligations</b></div>
                           </div>
                           <div class="row margin-top text-center">
                              <div class="col-xs-12" style="border-bottom: 1px solid black; width:95%;">
                                 &nbsp;
                              </div>
                           </div>
                           <div class="row text-center">
                              <div class="col-xs-12">
                                 Librarian, Technology Information and Promotion Section
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-12"><b>4.  Employee Association Obligations</b></div>
                           </div>
                           <div class="row margin-top text-center">
                              <div class="col-xs-12" style="border-bottom: 1px solid black; width:95%;">
                                 &nbsp;
                              </div>
                           </div>
                           <div class="row text-center">
                              <div class="col-xs-12">
                                 President, SALEM 
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-12"><b>6.  Financial Obligations</b></div>
                           </div>
                           <div class="row margin-top text-center">
                              <div class="col-xs-12" style="border-bottom: 1px solid black; width:95%;">
                                 &nbsp;
                              </div>
                           </div>
                           <div class="row text-center">
                              <div class="col-xs-12">
                                 Accountant IV
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-12"><b>8.  Records and Personnel Matters </b></div>
                           </div>
                           <div class="row margin-top text-center">
                              <div class="col-xs-12" style="border-bottom: 1px solid black; width:95%;">
                                 &nbsp;
                              </div>
                           </div>
                           <div class="row text-center">
                              <div class="col-xs-12">
                                 Chief, Administrative and General Services Section
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-12"><b>DOST- HRDP</b></div>
                           </div>
                           <div class="row margin-top text-center">
                              <div class="col-xs-12" style="border-bottom: 1px solid black; width:95%;">
                                 &nbsp;
                              </div>
                           </div>
                           <div class="row text-center">
                              <div class="col-xs-12">
                                 Chairperson
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-12"><b>&nbsp;</b></div>
                           </div>
                           <div class="row margin-top text-center">
                              <div class="col-xs-12" style="border-bottom: 1px solid black; width:95%;">
                                 &nbsp;
                              </div>
                           </div>
                           <div class="row text-center">
                              <div class="col-xs-12">
                                 Deputy Executive Director 
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row margin-top" style="padding: 5px;">
                        <div class="col-xs-12">
                           <div class="row">
                              <div class="col-xs-12">
                                 <b>APPROVED:</b>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-12">
                                 <input type="checkbox" disabled>&nbsp;
                                 Without prejudice to any current financial/contractual obligations with the MIRDC/DOST in view of fellowship/scholarship grants, loans and other accounts payable.
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-12">
                                 <input type="checkbox" disabled>&nbsp;
                                 Without prejudice to any financial/contractual obligation with the Center and other government agencies that may be found in the future. 
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-6" style="border-bottom: 1px solid black;">
                                 &nbsp;
                              </div>
                              <div class="col-xs-6" style="border-bottom: 1px solid black;">
                                 &nbsp;
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-xs-6 text-center">
                                 Executive Director
                              </div>
                              <div class="col-xs-6 text-center">
                                 Date
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <?php
                  //}
               }
            }
         ?>
      </div>
   </body>
</html>