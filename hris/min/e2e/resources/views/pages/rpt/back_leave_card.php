<?php 
      $where = "where EmployeesRefId = ".$row["RefId"]." AND Status = 'Approved'";
      $rsLeave = SelectEach("employeesleave",$where);
      if ($rsLeave) {
         $vl_month    = 0;
         $vl_new_val  = 0;
         $sl_month    = 0;
         $sl_new_val  = 0;
         while ($row = mysqli_fetch_assoc($rsLeave)) {
            $dfrom = date("d",strtotime($row["ApplicationDateFrom"]));
            $dto = date("d",strtotime($row["ApplicationDateTo"]));
            
?>    
            <tr>
               <td>
                  <?php
                     echo date("F",strtotime($row["ApplicationDateFrom"]));
                  ?>
               </td>
               <td>
                  <?php
                     echo "(".$dfrom."-".$dto.") ".getRecord("leaves",$row["LeavesRefId"],"Name");
                  ?>
               </td>
               <td class="text-center">
                  <?php
                     $month = date("m",strtotime($row["ApplicationDateFrom"]));
                     $year  = date("Y",strtotime($row["ApplicationDateFrom"]));
                     $lastday = cal_days_in_month(CAL_GREGORIAN,$month,$year);
                     $final_date = $year."-".$month."-".$lastday;
                     $first_date = $year."-".$month."-01";
                     $arr = getDTRSummary($first_date,
                                          $final_date,
                                          $row["EmployeesRefId"],
                                          $row["CompanyRefId"],
                                          $row["BranchRefId"]);
                     echo $arr["VLEarned"];
                  ?>
               </td>
               <td class="text-center">
                  <?php
                     $vl_ual = $arr["UAL"];
                     echo $vl_ual;
                  ?>
               </td>
               <td class="text-center">
                  <?php
                     if ($vl_month != $vl_ual) {
                        $vl_new_val = ${"arr_".$EmployeesRefId}["VLBegBal"] + $arr["VLEarned"] - $vl_ual;
                        $vl_month = $vl_ual;
                     } else if ($vl_month == 0) {
                        $vl_new_val = ${"arr_".$EmployeesRefId}["VLBegBal"] + $arr["VLEarned"];
                        $vl_month = 0;
                     } else {
                        $vl_new_val = ${"arr_".$EmployeesRefId}["VLBegBal"];
                     }
                     echo $vl_new_val;
                     ${"arr_".$EmployeesRefId}["VLBegBal"] = $vl_new_val;
                  ?>
               </td>
               <td class="text-center">
               </td>
               <td class="text-center">
                  <?php
                     $month = date("m",strtotime($row["ApplicationDateFrom"]));
                     $year  = date("Y",strtotime($row["ApplicationDateFrom"]));
                     $lastday = cal_days_in_month(CAL_GREGORIAN,$month,$year);
                     $final_date = $year."-".$month."-".$lastday;
                     $first_date = $year."-".$month."-01";
                     $arr = getDTRSummary($first_date,
                                          $final_date,
                                          $row["EmployeesRefId"],
                                          $row["CompanyRefId"],
                                          $row["BranchRefId"]);
                     echo $arr["SLEarned"];
                  ?>
               </td>
               <td class="text-center">
                  <?php
                     if (getRecord("leaves",$row["LeavesRefId"],"Code") == "SL") {
                        $diff = dateDifference($row["ApplicationDateFrom"],$row["ApplicationDateTo"]);
                        echo $diff;
                        $sl_ual = $diff;
                     }
                  ?>
               </td>
               <td class="text-center">
                  <?php
                     //echo $sl_new_val."----><br>";
                     $sl_begbal = ${"arr_".$EmployeesRefId}["SLBegBal"];
                     $sl_earned = $arr["SLEarned"];
                     $sl_leave   = $sl_ual;

                     if ($sl_month != $sl_ual) {
                        $sl_new_val = $sl_begbal + $sl_earned - $sl_leave;
                        $sl_month   = $sl_ual;
                     } else if ($sl_month != 0) {
                        $sl_new_val = $sl_begbal + $sl_earned;
                        $sl_month = 0;
                     }/* else {
                        $sl_new_val = $sl_begbal;
                     }*/
                     echo $sl_new_val;
                     ${"arr_".$EmployeesRefId}["SLBegBal"] = $sl_new_val;
                     /*echo getCreditBalance(date("m",strtotime($row["ApplicationDateFrom"])),
                                          "SL",
                                          $CompanyRefId,
                                          $BranchRefId,
                                          $EmployeesRefId,
                                          "Balance",
                                          date("Y",time())
                                       ); 
                     */
                  ?>
               </td>
               <td class="text-center">
               </td>
               <td>
                  
               </td>
            </tr>
<?php
         } 
      } else {
         echo 
         '<tr>
            <td colspan="11">NO RECORD FOUND</td>
         </tr>';
      }
?>