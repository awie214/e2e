<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $table = "employees";
   $whereClause .= " ORDER BY LastName";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) {
      echo $whereClause;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
      <style type="text/css">
         @media print {
            body {
               font-size: 9pt;
            }
            thead {
               font-size: 9pt;  
            }
            tbody {
               font-size: 8pt !important;
            }
         }
      </style>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <div class="col-xs-12">
            <div class="row">
               <div class="col-xs-6">
                  <b><i>CS Form No. 2</i></b>
                  <br>
                  <i>Revised 2018</i>
               </div>
               <div class="col-xs-2"></div>
               <div class="col-xs-4 text-center" style="border: 2px solid black;">
                  <b><i>For Use of Accredited Agencies Only</i></b>
               </div>
            </div>
            <div class="row margin-top">
               <div class="col-xs-12 text-center">
                  <br><br>
                  <span style="font-size: 15pt;"><b>REPORT ON APPOINTMENTS ISSUED (RAI)</b></span>
                  <br>
                  For the month of ___________________________ <?php echo date("Y",time()); ?>
               </div>
            </div>
            <div class="row margin-top">
               <div class="col-xs-12 text-right">
                  Date received by CSCFO:___________________________________
               </div>
            </div>
            <div class="row margin-top">
               <div class="col-xs-4">
                  <b>AGENCY: </b><u>Philippine Institute for Development Studies</u>
               </div>
               <div class="col-xs-4">
                  <b>CSC Resolution No: </b>_______________________
               </div>
               <div class="col-xs-4">
                  <b>CSCFO In-charge: </b>_______________________________
               </div>
            </div>
            <br>
            <div class="row margin-top">
               <div class="col-xs-1">
                  INSTRUCTIONS:
               </div>
               <div class="col-xs-11">
                  (1) Fill-out the data needed in the form completely and accurately.
                  <br>
                  (2) Do not abbreviate entries in the form.
                  <br>
                  (3) Accomplish the Checklist of Common Requirements and sign the certification.
                  <br>
                  (4) Submit the duly accomplished form in electronic and printed copy (2 copies) to the CSC Field Office-in-Charge 
                  <br>
                       together with the original CSC copy of appointments and supporting documents within the 30th day of the succeeding month. 
               </div>
            </div>
            <div class="row margin-top">
               <div class="col-xs-12">
                  <br><br>
                  <b>Pertinent data on appointment issued</b>
               </div>
            </div>
            <div class="row margin-top">
               <div class="col-xs-12">
                  <table width="100%">
                     <thead>
                        <tr class="colHEADER">
                           <th rowspan="2">
                              Date Issued/
                              <br>
                              Effectivity
                              <br>
                              (mm/dd/yyyy)
                           </th>
                           <th colspan="4">
                              NAME OF APPOINTEE/S
                           </th>
                           <th rowspan="2">
                              POSITION
                              <br>
                              TITLE
                              <br>
                              (Indicate
                              <br>
                              parenthetical
                              <br>
                              title if
                              <br>
                              applicable
                              )
                           </th>
                           <th rowspan="2">
                              ITEM NO.
                           </th>
                           <th rowspan="2">
                              SALARY/
                              <br>
                              JOB/
                              <br>
                              PAY
                              <br>
                              GRADE
                           </th>
                           <th rowspan="2">
                              EMPLOYMENT
                              <br>
                              STATUS
                           </th>
                           <th rowspan="2">
                              PERIOD OF
                              <br>
                              EMPLOYMENT (for
                              <br>
                              Temporary, Casual/
                              <br>
                              Contractual Appointments)
                              <br>
                              (mm/dd/yyyy to mm/dd/yyyy)
                           </th>
                           <th rowspan="2">
                              NATURE OF
                              <br>
                              APPOINTMENT
                           </th>
                           <th colspan="2">
                              PUBLICATION
                           </th>
                           <th colspan="3">
                              CSC ACTION
                           </th>
                           <th rowspan="2">
                              Agency
                              <br>
                              Receiving
                              <br>
                              Officer
                           </th>
                        </tr>
                        <tr class="colHEADER">
                           <th>Last Name</th>
                           <th>First Name</th>
                           <th>
                              Name
                              <br>
                              Extension
                              <br>
                              (Jr./III)
                           </th>
                           <th>
                              Middle
                              <br>
                              Name
                           </th>
                           <th>
                              DATE
                              <br>
                              indicate period
                              <br>
                              of publication
                              <br>
                              (mm/dd/yyyy to
                              <br>
                              mm/dd/yyyy)
                           </th>
                           <th>
                              MODE
                              <br>
                              (CSC Bulletin of
                              <br>
                              Vacant
                              <br>
                              Positions,
                              <br>
                              Agency Website,
                              <br>
                              Newspaper, etc) 
                           </th>
                           <th>
                              V-Validated
                              <br>
                              INV-
                              <br>
                              Invalidated
                              <br>
                              N-Noted
                           </th>
                           <th>
                              Date
                              <br>
                              Of
                              <br>
                              Action
                              <br>
                              (mm/dd/yyyy)
                           </th>
                           <th>
                              Date
                              <br>
                              Of
                              <br>
                              Release
                              <br>
                              (mm/dd/yyyy)
                           </th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php
                           for ($i=1; $i <= 10 ; $i++) { 
                              echo '
                                 <tr>
                                    <td>&nbsp;</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                 </tr>
                              ';
                           }
                        ?>
                     </tbody>
                  </table>
               </div>
            </div>
            <br>
            <div class="row margin-top">
               <div class="col-xs-4">
                  <div class="row">
                     <div class="col-xs-12">
                        <b>
                           CERTIFICATION:
                        </b>
                     </div>
                  </div>
                  <br>
                  <br>
                  <div class="row">
                     <div class="col-xs-12 text-center">
                        This is to certify that the information contained in this           
                        report are true, correct and complete based on the Plantilla            
                        of Personnel and appointment/s issued.
                        <br>
                        <br>
                        <u>
                           JOHN ZERNAN B. LUNA
                        </u>
                        <br>
                        HRMO
                     </div>
                  </div>
               </div>
               <div class="col-xs-4">
                  <div class="row">
                     <div class="col-xs-12">
                        <b>
                           CERTIFICATION:
                        </b>
                     </div>
                  </div>
                  <br>
                  <br>
                  <div class="row">
                     <div class="col-xs-12 text-center">
                        This is to certify that the appointment/s issued        
                        is/are in accordance with existing Civil Service Law,          
                        rules and regulations. 
                        <br>
                        <br>
                        <u>
                           ANDREA S. AGCAOILI
                        </u>       
                        <br>
                        Department Manager III
                        <br>
                        Administrative and Finance Department
                     </div>
                  </div>
               </div>
               <div class="col-xs-4">
                  <div class="row">
                     <div class="col-xs-12">
                        <b>
                           Post-Audited by:
                        </b>
                     </div>
                  </div>
                  <br>
                  <br>
                  <div class="row">
                     <div class="col-xs-12 text-center" style="opacity: 0;">
                        This is to certify that the appointment/s issued        
                        is/are in accordance with existing Civil Service Law,          
                        rules and regulations.        

                     </div>
                  </div>
                  <div class="row">
                     <div class="col-xs-12 text-center">
                        ______________________________
                        <br>
                        CSC Official
                     </div>
                  </div>
               </div>
            </div>
            <br>
            <div class="row margin-top">
               <div class="col-xs-12">
                  <b>For CSC Use Only:</b>
                  <br>
                  <table width="100%">
                     <tr>
                        <td>
                           <b>REMARKS/COMMENTS/RECOMMENDATIONS (e.g. Reasons for Invalidation):</b>
                        </td>
                     </tr>
                     <tr>
                        <td>&nbsp;</td>
                     </tr>
                     <tr>
                        <td>&nbsp;</td>
                     </tr>
                  </table>
               </div>
            </div>
            <div class="row">
               <div class="col-xs-12 text-right">
                  <b>Page 1 of _________</b>
               </div>
            </div>
         </div>
      </div>
   </body>
</html>