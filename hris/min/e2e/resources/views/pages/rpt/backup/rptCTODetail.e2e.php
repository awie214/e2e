<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include "pageHEAD.e2e.php"; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            rptHeader(getRptName(getvalue("drpReportKind")));
         ?>
         <p class="txt-center">As of : <u><?php echo monthName(date("m",time()),1).", ".date("Y",time()); ?></u></p>
         <br>
         <p>Name of Employee : <u>DELA CRUZ, JUAN</u></p>

         <table border="1">
            <tr>
               <th>DATE OF COC</th>
               <th>EARNINGS</th>
               <th>DATE OF CTO</th>
               <th>NO. OF HOURS</th>
               <th>REMAINING COC/s BALANCE</th>
            </tr>
            <?php for($j=1;$j<=7;$j++) {?>
            <tr>
               <td class="txt-center">&nbsp;</td>
               <td class="txt-center"></td>
               <td class="txt-center"></td>
               <td class="txt-center"></td>
               <td class="txt-center"></td>
            </tr>
            <?php } ?>
            <tr>
               <td class="txt-right" colspan=4>TOTAL (Balance of COC)</td>
               <td class="txt-right">0.00</td>
            </tr>
         </table>
      </div>
      <?php rptFooter(); ?>
   </body>
</html>