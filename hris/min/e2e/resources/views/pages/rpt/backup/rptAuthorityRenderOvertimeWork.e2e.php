<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include "pageHEAD.e2e.php"; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
      <style>
         .bottomline {border-bottom:2px solid black;}
         .bold {font-size:600;}
         table (clear:both;)
      </style>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            rptHeader("");
            echo
            '<div class="txt-center bold"><h5>'.strtoupper(getRptName(getvalue("drpReportKind"))).'</h5></div>';
         ?>

         <div class="row">
            <div class="col-xs-1 txt-right">Name: </div>
            <div class="col-xs-6 bottomline">&nbsp;</div>
            <div class="col-xs-2 txt-right">Date(s) of Overtime: </div>
            <div class="col-xs-3 bottomline">&nbsp;</div>
         </div>
         <br>
         <table>
            <tr>
               <td style="width:150px;" nowrap>Purpose of Overtime:</td>
               <td class="bottomline">&nbsp;</td>
            </tr>
            <tr><td class="bottomline" colspan=2>&nbsp;</td></tr>
            <tr><td class="bottomline" colspan=2>&nbsp;</td></tr>
            <tr>
               <td style="width:150px;" nowrap>Expected Output:</td>
               <td class="bottomline">&nbsp;</td>
            </tr>
            <tr><td class="bottomline" colspan=2>&nbsp;</td></tr>
            <tr><td class="bottomline" colspan=2>&nbsp;</td></tr>
         </table>
         <br>
         <table>
            <tr>
               <td class="txt-right" nowrap style="width:16%">REQUESTED BY:</td>
               <td class="bottomline" style="width:16%">&nbsp;</td>
               <td>&nbsp;</td>
               <td class="txt-right" nowrap style="width:16%">APPROVED BY:</td>
               <td class="bottomline" style="width:16%">&nbsp;</td>
               <td>&nbsp;</td>
            </tr>
            <tr>
               <td nowrap></td>
               <td class="txt-center">Name</td>
               <td>&nbsp;</td>
               <td nowrap></td>
               <td class="txt-center">Name</td>
               <td>&nbsp;</td>
            </tr>
            <tr>
               <td class="txt-center">&nbsp;</td>
               <td class="txt-center bottomline"></td>
               <td>&nbsp;</td>
               <td class="txt-center">&nbsp;</td>
               <td class="txt-center bottomline"></td>
               <td>&nbsp;</td>
            </tr>
            <tr>
               <td>&nbsp;</td>
               <td class="txt-center">Designation</td>
               <td>&nbsp;</td>
               <td>&nbsp;</td>
               <td class="txt-center">Designation</td>
               <td>&nbsp;</td>
            </tr>
         </table>
         <hr>
         <p style="text-indent:100px;">CERTIFICATE OF ACCOMPLISHMENT</p>
         <p style="text-indent:120px;">(to be accomplished by Recruting Officer)</p><br>
         <p style="text-indent:120px;"><i>This is to certify that the purpose of overtime and/or required output/s a herein authorized hs been accomplished</i></p>
         <p style="text-align:right">Name:_______________________</p>
         <p style="text-align:right">Designation:_______________________</p>
         <hr>
         <p style="text-indent:100px;">COMPUTATION OF OVERTIME PAY</p>
         <p style="text-indent:120px;">(to be accomplished by Accounting Unit)</p><br>
         <p>
            <span>Date of Overtime Work: _________________________________</span>
            <span style="float:right">Actual No. Overtime Hours: _________________________________</span>
         </p>
         <p>
            <span>Total Overtime Pay: _____________________________</span>
            <span style="float:right">Less Deductions: _______________ Net Overtime Pay __________________</span>
         </p>
         <br>
         <br>
         <p style="text-align:right">Computed By:_______________________</p>


      </div>
      <?php rptFooter(); ?>
   </body>
</html>