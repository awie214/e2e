<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            rptHeader(getRptName(getvalue("drpReportKind")));
         ?>

         <div style="padding:1in;border:1px solid #000;">
            <p>
               This certificate entitles Mr./Ms ______________________________________ to _______________________________ of Compensatory Overtime.
            </p>
            <div class="txt-right">______________________</div>
            <div class="txt-right" style="padding-right:50px;">ALO-HRDD</div>

            <br>
            <br>
            <br>
            <p>Date Issued: ____________________________</p>
            <p>Valid Until: ____________________________</p>
         </div>
         <br>
            <br>
            <br>

         <table border="1">
            <tr>
               <th>No. of Hours Earned <br>COC/s Beginning Balance</th>
               <th>DATE OF CTO</th>
               <th>Used COC/s</th>
               <th>REMAINING COC/s</th>
               <th>Remarks</th>
            </tr>
            <?php for($j=1;$j<=7;$j++) {?>
            <tr>
               <td class="txt-center">&nbsp;</td>
               <td class="txt-center"></td>
               <td class="txt-center"></td>
               <td class="txt-center"></td>
               <td class="txt-center"></td>
            </tr>
            <?php } ?>
         </table>
         <p>
            <div class="row">
               <div class="col-xs-2 txt-right">Approved By:</div>
               <div class="col-xs-4"></div>
               <div class="col-xs-2 txt-right">Claimed By:</div>
               <div class="col-xs-4"></div>
            </div>
            <div class="row">
               <div class="col-xs-2"></div>
               <div class="col-xs-4">________________________</div>
               <div class="col-xs-2"></div>
               <div class="col-xs-3">________________________</div>
               <div class="col-xs-1"></div>
            </div>
            <div class="row">
               <div class="col-xs-2"></div>
               <div class="col-xs-4">Head of Office</div>
               <div class="col-xs-2"></div>
               <div class="col-xs-3">ALO-HRDD</div>
               <div class="col-xs-1"></div>
            </div>
            <br><br><br>
            <div class="row">
               <div class="col-xs-2"></div>
               <div class="col-xs-4">________________________</div>
               <div class="col-xs-2"></div>
               <div class="col-xs-3">________________________</div>
               <div class="col-xs-1"></div>
            </div>
            <div class="row">
               <div class="col-xs-2"></div>
               <div class="col-xs-4">Date</div>
               <div class="col-xs-2"></div>
               <div class="col-xs-3">Date</div>
               <div class="col-xs-1"></div>
            </div>
         </p>

      </div>
   </body>
</html>