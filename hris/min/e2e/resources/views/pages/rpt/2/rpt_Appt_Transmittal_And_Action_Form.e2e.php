<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include_once 'incRptParam.e2e.php';
   include_once 'incRptQryString.e2e.php';
   $rsEmployees = SelectEach("employees",$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) { echo "DBG >> ".$whereClause; }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <table>
            <thead>
               <tr>
                  <th colspan="16" align="center" style="text-align:center;">
                     <?php
                        rptHeader(getRptName(getvalue("drpReportKind")));
                     ?>
                  </th>
               </tr>
               <tr>
                  <th colspan="7" class="text-left">Agency: ____________________________________________</th>
                  <th colspan="2">&nbsp;</th>
                  <th colspan="7" class="text-right">CSFCO In-charge: ____________________________________________</th>
               </tr>   
               <tr>
                  <th colspan="7" class="text-left">
                     <div class="row">
                        INSTRUCTIONS:
                     </div>
                     <div class="row margin-top">
                        (1) Fill-out the data needed in the form completely and accurately.
                     </div>
                     <div class="row margin-top">
                        (2) Do not abbreviate entries in the form.
                     </div>
                     <div class="row margin-top">
                        (3) Accomplish the Checklist of Common Requirements and sign the certification.
                     </div> 
                     <div class="row margin-top">
                        (4) Submit the duty accomplished form in the electronic and printed copy (2 copies) to the CSC Field Office-In-Charge together with the original copies of the appointments and supporting documents.
                     </div>
                  </th>
                  <th colspan="2">&nbsp;</th>
                  <th colspan="7" class="text-right">
                     <div class="row" style="padding: 5px;">
                        <div class="col-xs-12">
                           <div class="row" style="border: 1px solid black;"> 
                              <div class="col-xs-12">
                                 <div class="row" style="background: gray; border-bottom: 1px solid black; text-align: left;"> 
                                    <i>For CSCRO/FO's Use:</i>
                                 </div>
                                 <div class="row" style="height: 100px; margin-bottom: 20px;"></div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </th>
               </tr>
               <tr class="colHEADER">
                  <th rowspan="2">NAME</th>
                  <th rowspan="2">POSITION TITLE</th>
                  <th rowspan="2">SALARY / JOB</th>
                  <th rowspan="2">EMPLOYMENT STATUS</th>
                  <th colspan="2">
                     PERIOD OF APPOINTMENT
                     <br>
                     (for Temporary, Casual / Contractaul Appointment)
                  </th>
                  <th rowspan="2">NATURE OF APPOINTMENT</th>
                  <th rowspan="2">DATE OF ISSUANCE</th>
                  <th colspan="3">PUBLICATION</th>
                  <th colspan="3">CSC ACTION</th>
                  <th rowspan="2">Agency Receiving Officer</th>
               </tr>
               <tr class="colHEADER">
                  <th>DATE<br>FROM</th>
                  <th>DATE<br>TO</th>
                  <th>DATE<br>FROM</th>
                  <th>DATE<br>TO</th>
                  <th>
                     MODE
                     <br>
                     (CSC Bulletin of Vacant Positions)
                  </th>
                  <th>APPROVED<br>OR<br>DISAPPROVED</th>
                  <th>Date of Action</th>
                  <th>Date of Release</th>
               </tr>
            </thead>
            <tbody>
                  <?php
                     while ($row = mysqli_fetch_assoc($rsEmployees)) {
                        $EmployeesRefId = $row["RefId"];
                        $CompanyRefId   = $row["CompanyRefId"];
                        $BranchRefId    = $row["BranchRefId"];
                        $where  = "WHERE CompanyRefId = $CompanyRefId";
                        $where .= " AND BranchRefId = $BranchRefId";
                        $where .= " AND EmployeesRefId = $EmployeesRefId";
                        $empinfo_row = FindFirst("empinformation",$where,"*");
                        if ($empinfo_row) {
                           $Position = getRecord("position",$empinfo_row["PositionRefId"],"Name");
                           $SalaryGrade = getRecord("salarygrade",$empinfo_row["SalaryGradeRefId"],"Name");
                           $JobGrade = getRecord("JobGrade",$empinfo_row["JobGradeRefId"],"Name");
                           $EmpStatus = getRecord("EmpStatus",$empinfo_row["EmpStatusRefId"],"Name");

                        } else {
                           $Position = "";
                           $SalaryGrade = "";
                           $JobGrade = "";
                           $EmpStatus = "";
                        }
                   ?>
                     <tr>
                        <td><?php echo $row["LastName"].", ".$row["FirstName"]." ".$row["MiddleName"]; ?></td>
                        <td>
                           <?php
                              echo $Position;
                           ?>
                        </td>
                        <td class="text-center">
                           <?php
                              if ($SalaryGrade != "" && $JobGrade != "") {
                                 echo $SalaryGrade." / ".$JobGrade;
                              } else {
                                 if ($SalaryGrade != "") {
                                    echo $SalaryGrade;
                                 }
                                 if ($JobGrade != "") {
                                    echo $JobGrade;
                                 }
                              }
                              
                           ?>
                        </td>
                        <td> 
                           <?php
                              echo $EmpStatus;
                           ?>
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                     </tr>
                  <?php 
                  } ?>
               <tr>
                  <td colspan="2">&nbsp;</td>
                  <td colspan="14">
                     <br>
                     <label>CERTIFICATION:</label>
                     <br>
                     This is to certify that the information contained in this form are true, correct and complete.
                     <br>
                     <div class="row">
                        <div class="col-xs-6">
                           _______________________________________
                           <br>
                           Highest Ranking HRMO
                        </div>
                     </div>
                     <div class="row margin-top"> 
                        <div class="col-xs-12">
                           DATE: _______________________________________
                        </div>
                     </div>
                  </td>
               </tr>
               <tr>
                  <td colspan="16" style="padding: 10px;">
                     <div class="row" style="border: 1px solid black; padding: 5px; height: 100px; margin-top: 20px;">
                        <div class="col-xs-12">
                           <label>REMARKS / COMMENTS / RECOMMENDATIONS: (eg. Reasons for Disapproval of Appointment)</label>
                        </div>
                     </div>
                     <?php spacer(50); ?>
                     <div class="row margin-top">
                        <table>
                           <tr>
                              <td colspan="2" style="width: 50%; text-align: center;"><label>CHECKLIST OF COMMON REQUIREMENTS</label></td>
                              <td style="width: 25%; text-align: center;"><label>HRMO</label></td>
                              <td style="width: 25%; text-align: center;"><label>CSC FO</label></td>
                           </tr>
                           <tr>
                              <td colspan="4">
                                 Instructions: Put a check if the requirements are complete. If incomplete, use the space provided to indicate the name of appointee and the lacking requirement/s.
                              </td>
                           </tr>
                           <tr>
                              <td>1</td>
                              <td>
                                 <b>APPOINTMENT FORMS</b> (CS Form No. 33-A, Revised 2017) - Three (3) original copies of appointment form (employee copy, CSC copy and agency copy)
                              </td>
                              <td></td>
                              <td></td>
                           </tr>
                           <tr>
                              <td>2</td>
                              <td>
                                 <b>PLANTILLA OF CASUAL APPOINTMENT</b> (CSC Form No. 34-A or C) - Three (3) original copies (employee copy, CSC copy and agency copy)
                              </td>
                              <td></td>
                              <td></td>
                           </tr>
                           <tr>
                              <td>3</td>
                              <td>
                                 <b>PERSONAL DATA SHEET</b> (CS Form No. 212, Revised 2017)
                              </td>
                              <td></td>
                              <td></td>
                           </tr>
                           <tr>
                              <td>4</td>
                              <td>
                                 <b>ORIGINAL COPY OF AUTHENTICATED CERTIFICATE OF ELIGIBILITY/ RATING/ LICENSE</b> - Except if the eligibility has been previously authenticated in 2004 or onward and recorded by the CSC
                              </td>
                              <td></td>
                              <td></td>
                           </tr>
                           <tr>
                              <td>5</td>
                              <td>
                                 <b>POSITION DESCRIPTION FORM</b> (DBM-CSC Form No. 1,  Revised 2017) 
                              </td>
                              <td></td>
                              <td></td>
                           </tr>
                           <tr>
                              <td>6</td>
                              <td>
                                 <b>OATH OF OFFICE</b> (CS Form No. 32,  Revised 2017)
                              </td>
                              <td></td>
                              <td></td>
                           </tr>
                           <tr>
                              <td>7</td>
                              <td>
                                 <b>CERTIFICATE OF ASSUMPTION TO DUTY</b> (CS Form No. 4) 
                              </td>
                              <td></td>
                              <td></td>
                           </tr>
                           <tr>
                              <td colspan="2"></td>
                              <td class="text-center">
                                 This is to certify that I have checked the veracity, authenticity and completeness of all the requirements in support of the appointments attached herein.
                                 <br><br><br>
                                 ________________________________
                                 Highest Ranking HRMO
                              </td>
                              <td class="text-center">
                                 This is to certify that I have checked all the requirements in support of the appointments attached herein and found these to be  [  ] complete /  [  ]   lacking.
                                 <br><br><br>
                                 ________________________________
                                 CSC FO Receiving Officer
                              </td>
                           </tr>
                        </table>
                     </div>
                  </td>
               </tr>
            </tbody>
         </table>   
      </div>
   </body>
</html>