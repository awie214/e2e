<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $table = "employees";
   $whereClause .= " ORDER BY LastName";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) {
      echo $whereClause;
   }
   $this_year = date("Y",time());
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            $count = 0;
            if ($rsEmployees) {
         ?>
            <table style="width: 100%;">
               <thead>
                  <tr>
                     <td colspan="15">
                        <?php
                           rptHeader(getvalue("RptName"));
                        ?>
                     </td>
                  </tr>
                  <tr class="colHEADER">
                     <th>No.</th>
                     <th style="width: 20%;">EMPLOYEE NAME</th>
                     <th>JAN</th>
                     <th>FEB</th>
                     <th>MAR</th>
                     <th>APR</th>
                     <th>MAY</th>
                     <th>JUN</th>
                     <th>JULY</th>
                     <th>AUG</th>
                     <th>SEPT</th>
                     <th>OCT</th>
                     <th>NOV</th>
                     <th>DEC</th>
                     <th>TOTAL</th>
                  </tr>
               </thead>
               <tbody>
                  <?php
                     while ($row_emp = mysqli_fetch_assoc($rsEmployees)) {
                        $count++;
                        $total_sl = 0;
                        $FullName   = $row_emp["LastName"].", ".$row_emp["FirstName"]." ".$row_emp["MiddleName"];
                        for ($i=1; $i <= 12 ; $i++) { 
                           ${$i."_"} = 0;
                        }
                        $emp_leave = SelectEach("employeesleave","WHERE EmployeesRefId = ".$row_emp["RefId"]." AND Status = 'Approved'");
                        if ($emp_leave) {
                           while ($leave_row = mysqli_fetch_assoc($emp_leave)) {
                              $year = date("Y",strtotime($leave_row["ApplicationDateFrom"]));
                              if ($year == $this_year) {
                                 $diff = dateDifference($leave_row["ApplicationDateFrom"],$leave_row["ApplicationDateTo"]) + 1;
                                 $month = intval(date("m",strtotime($leave_row["ApplicationDateFrom"])));
                                 ${$month."_"} = ${$month."_"} + $diff;
                                 $total_sl = $total_sl + $diff;
                              }
                           }
                        }
                        echo '
                           <tr>
                              <td class="text-center">'.$count.'</td>
                              <td>'.$FullName.'</td>
                        ';
                        for ($a=1; $a <=12 ; $a++) { 
                           if (${$a."_"} == 0) {
                              ${$a."_"} = "&nbsp;";
                           }
                           echo '<td class="text-center">'.${$a."_"}.'</td>';
                        }
                        echo '<td class="text-center">'.$total_sl.'</td>';
                        echo '</tr>';
                     }
                  ?>
               </tbody>
            </table>
         
         <?php
            }
         ?>
      </div>
   </body>
</html>