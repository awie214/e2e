<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include_once 'incRptQryString.e2e.php';

   function getCreditBalance($month,$type,$CompanyRefId,$BranchRefId,$EmployeesRefId,$fld,$year) {
      include "conn.e2e.php";
      $lastday = cal_days_in_month(CAL_GREGORIAN,$month,date("Y",time()));
      $final_date = $year."-".$month."-".$lastday;
      $first_date = $year."-".$month."-01";
      $where = "WHERE CompanyRefId = $CompanyRefId AND BranchRefId = $BranchRefId AND EmployeesRefId = $EmployeesRefId";
      $where .= " AND NameCredits = '".$type."'";
      $where .= " AND AccumDate >= '".$first_date."' AND AccumDate <= '".$final_date."'";
      $where .= " ORDER BY AccumDate DESC";
   
      $sql = "SELECT * FROM accumcreditbalance ".$where;
      $new_rs = mysqli_query($conn,$sql);
      $balance = 0;
      if (mysqli_num_rows($new_rs) > 0) {
         $new_row = mysqli_fetch_assoc($new_rs);
         $balance = $new_row["$fld"];  
      }
      return $balance;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            rptHeader(getRptName(getvalue("drpReportKind")));
         ?>
         <p class="txt-center">As of : <u><?php echo monthName(date("m",time()),1).", ".date("Y",time()); ?></u></p>
         <br>

         <?php
            $sql = "SELECT * FROM employees ".$whereClause;
            $rs = mysqli_query($conn,$sql);
            if (mysqli_num_rows($rs)) {
               while ($row = mysqli_fetch_assoc($rs)) {
                  $EmployeesRefId = $row["RefId"];
                  $CompanyRefId   = $row["CompanyRefId"];
                  $BranchRefId    = $row["BranchRefId"];
                  $LastName       = $row["LastName"];
                  $FirstName      = $row["MiddleName"];
                  $MiddleName     = $row["FirstName"];
                  $FullName       = $row["LastName"].", ".$row["FirstName"]." ".$row["MiddleName"];
         ?>


         <div>
            <p>Name of Employee : <u><?php echo ("$FullName");?></u></p>

            <table border="1">
               <tr>
                  <th>No. Of Hours Earned COC/s Beginning Balance</th>
                  <th>Earned COC/s</th>
                  <th>COC Balance</th>
               </tr>
               
               <?php for($j=1;$j<=12;$j++) {?>
               <tr>
                  <td class="txt-center"><?php echo monthName($j,1) ?></td>
                  <td class="txt-center"></td>
                  <td class="txt-center">
                     <?php 
                        echo getCreditBalance($j,"OT",$CompanyRefId,$BranchRefId,$EmployeesRefId,"Balance",date("Y",time())); 
                     ?>
                  </td>
               </tr>
               <?php } ?>
            </table>
            <p>
               <div class="row">
                  <div class="col-xs-2 txt-right">Prepared By:</div>
                  <div class="col-xs-4"></div>
                  <div class="col-xs-2 txt-right">Approved By:</div>
                  <div class="col-xs-4"></div>
               </div>
               <div class="row">
                  <div class="col-xs-2"></div>
                  <div class="col-xs-4">________________________</div>
                  <div class="col-xs-2"></div>
                  <div class="col-xs-3">________________________</div>
                  <div class="col-xs-1"></div>
               </div>
            </p>
         </div>

         <?php
               }
            }
         ?>






      </div>
   </body>
</html>