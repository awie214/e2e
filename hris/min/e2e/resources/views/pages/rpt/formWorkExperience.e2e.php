<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $refid = getvalue("refid");
   $row = FindFirst("employees_work_experience_attachments","WHERE RefId = $refid","*");
   if ($row) {
      $StartDate        = date("F d, Y",strtotime($row["StartDate"]));
      $EndDate          = date("F d, Y",strtotime($row["EndDate"]));
      $PositionRefId    = getRecord("position",$row["PositionRefId"],"Name");
      $AgencyRefId      = getRecord("agency",$row["AgencyRefId"],"Name");
      $OfficeRefId      = getRecord("office",$row["OfficeRefId"],"Name");
      $Supervisor       = $row["Supervisor"];
      $Location         = $row["Location"];
      $Accomplishments  = $row["Accomplishments"];
      $Duties           = $row["Duties"];
      if ($EndDate == "") $EndDate = "PRESENT";
   } else {
      $StartDate        = "";
      $EndDate          = "";
      $PositionRefId    = "";
      $AgencyRefId      = "";
      $OfficeRefId      = "";
      $Supervisor       = "";
      $Location         = "";
      $Accomplishments  = "";
      $Duties           = "";
   }

?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
   </head>
   <body>
      <div class="container-fluid rptBody">
         <div class="row">
            <div class="col-xs-12">
               <div class="row margin-top">
                  <div class="col-xs-12">
                     Attachment to CS Form No. 212
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <table border="1" style="width: 100%;">
                        <tr>
                           <td style="text-align: center; padding: 10px; font-size: 13pt; font-weight: 600; background: gray; color: white;">
                              <i>WORK EXPERIENCE SHEET</i>
                           </td>
                        </tr>
                        <tr>
                           <td>
                              <div class="row">
                                 <div class="col-xs-3">
                                    <div class="row">
                                       <div class="col-xs-12">
                                          <b><i>Instructions:</i></b>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-xs-9">
                                    <div class="row">
                                       <div class="col-xs-12">
                                          <i>
                                             1. Include only the work experiences relevant to the position being applied for.
                                             <br>
                                             2. The duration should include start and finish dates, if known, month in abbreviated form, if known, and year in full. For the current position, use the word Present, e.g., 1998-Present. Work experience should be listed starting with the most recent/present employment
                                          </i>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </td>
                        </tr>
                        <tr>
                           <td>
                              <div class="row">
                                 <div class="col-xs-12">
                                    <br>
                                    <ul>
                                       <li>Duration: 
                                          <?php
                                             echo $StartDate." - ".$EndDate;
                                          ?>
                                       </li>
                                       <li>Position: 
                                          <?php
                                             echo $PositionRefId;
                                          ?>
                                       </li>
                                       <li>Name of Office/Unit: 
                                          <?php
                                             echo $OfficeRefId;
                                          ?>
                                       </li>
                                       <li>Immediate Supervisor: 
                                          <?php echo $Supervisor; ?>
                                       </li>
                                       <li>Name of Agency/Organization and Location: 
                                          <?php 
                                             echo $AgencyRefId." ".$Location;
                                          ?>
                                       </li>
                                       <li>List of Accomplishments and Contributions (if any)</li>
                                       <div class="row">
                                          <div class="col-xs-12">
                                             <?php
                                                echo $Accomplishments;
                                             ?>
                                          </div>
                                       </div>
                                       <li>Summary of Actual Duties</li>
                                       <div class="row">
                                          <div class="col-xs-12">
                                             <?php
                                                echo $Duties;
                                             ?>
                                          </div>
                                       </div>
                                    </ul>
                                 </div>
                              </div>
                           </td>
                        </tr>
                     </table>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12 text-right">
                     _______________________________________________
                     <br>
                     (Signature over Printed Name of Employee/Applicant)
                     <br>
                     Date: __________________
                  </div>
               </div>
            </div>
         </div>
      </div>
   </body>
</html>