<?php 
	include 'FnUpload.php';
	$count = 0;
	$emp_array = [
		"SpouseLastName","SpouseFirstName","SpouseMiddleName","SpouseExtName","OccupationsRefId","EmployersName",
		"BusinessAddress","SpouseMobileNo","FatherLastName","FatherFirstName","FatherMiddleName","FatherExtName",
		"MotherLastName","MotherFirstName","MotherMiddleName"
	];
	$file = fopen("csv/35/family_info.csv", "r");
	while(!feof($file)) {
		$fldnval = "";
		$fld = "";
		$val = "";
		$str = fgets($file);
		$row = explode(",", $str);
		if (count($row) > 1) {
			$row[0] = intval($row[0]);
			$where = "WHERE AgencyId = '".$row[0]."'";
			$emp_row = FindFirst("employees",$where,"*",$conn);
			if ($emp_row) {
				$emprefid = $emp_row["RefId"];
				$AgencyId = $emp_row["AgencyId"];
				$count++;
				$fld .= "EmployeesRefId, CompanyRefId, BranchRefId, ";
				$val .= "'$emprefid', 35, 1, ";
				for ($a=1; $a <= 15; $a++) { 
					$value = clean($row[$a]);
					if ($emp_array[$a - 1] == "OccupationsRefId") {
						$value = saveFM("occupations","Name, ","'$value', ",$value);
					}
					if ($value != "") {
						$fld .= "`".$emp_array[$a - 1]."`, ";
						$val .= "'".$value."', ";
					}
				}

				$save_family = save("employeesfamily",$fld,$val);
				if (is_numeric($save_family)) {
					echo "Record Saved For $AgencyId.<br>";
				} else {
					echo "Error $AgencyId.<br>";
				}
			}
		}
	}
?>