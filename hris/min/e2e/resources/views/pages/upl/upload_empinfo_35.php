<?php
	include 'FnUpload.php';
	include '../conn.e2e.php';
	mysqli_query($conn,"TRUNCATE empinformation");
	// mysqli_query($conn,"TRUNCATE position");
	// mysqli_query($conn,"TRUNCATE office");
	// mysqli_query($conn,"TRUNCATE division");
	// mysqli_query($conn,"TRUNCATE empstatus");
	// mysqli_query($conn,"TRUNCATE units");
	//mysqli_query($conn,"TRUNCATE employees");
	$obj 	= fopen("csv/empinfo_35.csv", "r");
	$count 	= 0;
	while(!feof($obj)) {
		$str = str_replace(",", "|", fgets($obj));
		if ($str != "") {
			$arr = explode("|", $str);
			$employee_fld 		= "";
			$employee_val 		= "";
			$plantilla_fldnval  = "";
			$empinfo_fld 		= "";
			$empinfo_val 		= "";
			$date_arr 			= explode("/", $arr[8]);
			$AgencyId 			= clean($arr[0]);	
			$LastName 			= strtoupper(clean($arr[1]));
			$FirstName 			= strtoupper(clean($arr[2]));
			$PositionRefId 		= clean($arr[3]);
			$OfficeRefId 		= clean($arr[4]);
			$DivisionRefId 		= clean($arr[5]);
			$UnitsRefId 		= clean($arr[6]);
			$PositionItemRefId 	= clean($arr[7]);
			
			$SalaryGradeRefId 	= clean($arr[9]);
			$StepIncrementRefId = clean($arr[10]);
			$SalaryAmount 		= clean($arr[11]);
			$EmpStatusRefId 	= clean($arr[12]);

			// if (strtolower($EmpStatusRefId) == "contract of service") {
			// 	echo ($SalaryAmount * 22)." -> $SalaryAmount<br>";
			// }
			if ($arr[8] != "") {
				$HiredDate 			= $date_arr[2]."-".$date_arr[0]."-".$date_arr[1];
				$empinfo_fld .= "`HiredDate`,`AssumptionDate`,";
				$empinfo_val .= "'$HiredDate','$HiredDate',";
			}
			if ($AgencyId != "") {
				$employee_fld .= "`AgencyId`,";
				$employee_val .= "'$AgencyId',";
			}
			if ($LastName != "") {
				$employee_fld .= "`LastName`,";
				$employee_val .= "'$LastName',";
			}
			if ($FirstName != "") {
				$employee_fld .= "`FirstName`,";
				$employee_val .= "'$FirstName',";
			}
			if ($SalaryAmount != "") {
				$empinfo_fld .= "`SalaryAmount`,";
				$empinfo_val .= "'$SalaryAmount',";
			}
			if ($PositionRefId != "" ) {
				$PositionRefId = saveFM("position","`Name`, ","'$PositionRefId', ",$PositionRefId);
				$empinfo_fld .= "`PositionRefId`, ";
				$empinfo_val .= "'$PositionRefId', ";
				$plantilla_fldnval .= "`PositionRefId` = '$PositionRefId',";
			}
			if ($OfficeRefId != "" ) {
				$OfficeRefId = saveFM("office","`Name`, ","'$OfficeRefId', ",$OfficeRefId);
				$empinfo_fld .= "`OfficeRefId`, ";
				$empinfo_val .= "'$OfficeRefId', ";
				$plantilla_fldnval .= "`OfficeRefId` = '$OfficeRefId',";
			}
			if ($DivisionRefId != "" ) {
				$DivisionRefId = saveFM("Division","`Name`, ","'$DivisionRefId', ",$DivisionRefId);
				$empinfo_fld .= "`DivisionRefId`, ";
				$empinfo_val .= "'$DivisionRefId', ";
				$plantilla_fldnval .= "`DivisionRefId` = '$DivisionRefId',";
			}
			if ($SalaryGradeRefId != "" ) {
				$SalaryGradeRefId = saveFM("SalaryGrade","`Name`, ","'$SalaryGradeRefId', ",$SalaryGradeRefId);
				$empinfo_fld .= "`SalaryGradeRefId`, ";
				$empinfo_val .= "'$SalaryGradeRefId', ";
				$plantilla_fldnval .= "`SalaryGradeRefId` = '$SalaryGradeRefId',";
			}
			if ($StepIncrementRefId != "") {
				$StepIncrementRefId = saveFM("StepIncrement","`Name`, ","'$StepIncrementRefId', ",$StepIncrementRefId);
				$empinfo_fld .= "`StepIncrementRefId`, ";
				$empinfo_val .= "'$StepIncrementRefId', ";
				$plantilla_fldnval .= "`StepIncrementRefId` = '$StepIncrementRefId',";
			}
			if ($EmpStatusRefId != "" ) {
				$EmpStatusRefId = saveFM("EmpStatus","`Name`, ","'$EmpStatusRefId', ",$EmpStatusRefId);
				$empinfo_fld .= "`EmpStatusRefId`, ";
				$empinfo_val .= "'$EmpStatusRefId', ";
			}
			if ($UnitsRefId != "" ) {
				$UnitsRefId = saveFM("Units","`Name`, ","'$UnitsRefId', ",$UnitsRefId);
				$empinfo_fld .= "`UnitsRefId`, ";
				$empinfo_val .= "'$UnitsRefId', ";
			}
			if ($plantilla_fldnval != "") {
				if ($PositionItemRefId != "") {
					$PositionItemRefId = saveFM("PositionItem","`Name`, ","'$PositionItemRefId', ",$PositionItemRefId);
					$empinfo_fld .= "`PositionItemRefId`, ";
					$empinfo_val .= "'$PositionItemRefId', ";
					$plantilla_fldnval .= "`SalaryAmount` = '$SalaryAmount',";
					$update_plantilla = update("positionitem",$plantilla_fldnval,$PositionItemRefId);
				}
			}
			// $save_employee = save("employees",$employee_fld,$employee_val);
			// if (is_numeric($save_employee)) {
			// 	$count++;
			// 	echo "$count. $FirstName $LastName Added.<br>";
			// }
			$emprefid = FindFirst("employees","WHERE AgencyId = '$AgencyId'","RefId",$conn);
			if ($emprefid) {
				$empinfo_fld .= "`EmployeesRefId`,";
				$empinfo_val .= "'$emprefid',";
				$save_empinfo = save("empinformation",$empinfo_fld,$empinfo_val);
				if (is_numeric($save_empinfo)) {
					$count++;
					echo "$count. $emprefid added employee info.<br>";
				}
			} else {
				echo "Cannot Find $AgencyId.<br>";
			}
		}
		
	}
?>