<?php
   include 'conn.e2e.php';
   $ttcount_arr = array();
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script type="text/javascript" src="<?php echo $_SESSION["login"] ?>"></script>
      <script type="text/javascript" src="<?php echo $path; ?>js/jsSHAver2/src/sha.js"></script>
      <link rel="stylesheet" href="<?php echo $path."/css/sideBar.css"; ?>">
      <script language="JavaScript">
         $(document).ready(function () {
         });
      </script>
   </head>
   <body>
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <nav class="navbar navbar-fixed-top">
            <div class="sysNameHolder sysBG" style="border-bottom:3px solid #fff;">
               <?php
                  $TRNBTN = 0;
                  $title = "";
                  $Logout = true;
                  include $files["inc"]["hdr"];
               ?>
               <span class="sysName">
                  <?php
                     echo $settings["Title"];
                  ?>
               </span>
            </div>
         </nav>
         <div style="margin-top:60px;">
            <?php
               if (!$isUser) {
                  doSideBarMain();
            ?>
                  <div class="container-fluid" id="mainScreen">
                     <?php doTitleBar("DASHBOARD / REMINDERS"); ?>
                     <div class="row">
                        <div class="col-xs-6 padd5">
                           <div class="panel panel-default">
                              <div class="panel-heading ">Employees Civil Status by Gender</div>
                              <div class="panel-body" style=" border: 1px solid #999999;">
                                 <div class="row" style="padding: 10px 10px 0px 10px;">
                                    <?php
                                       $M_Si = 0;
                                       $M_Ma = 0;
                                       $M_An = 0;
                                       $M_Wi = 0;
                                       $M_Se = 0;
                                       $M_Ot = 0;
                                       $F_Si = 0;
                                       $F_Ma = 0;
                                       $F_An = 0;
                                       $F_Wi = 0;
                                       $F_Se = 0;
                                       $F_Ot = 0;
                                       $F_ = 0;
                                       $_ = 0;
                                       $M_ = 0;
                                       $M = 0;
                                       $F = 0;
                                       $NoGender = 0;
                                       $rsEmployees = SelectEach("employees","");
                                       if (mysqli_num_rows($rsEmployees) > 0) {
                                          while ($row_emp = mysqli_fetch_assoc($rsEmployees)) {
                                             if ($row_emp["Sex"] == "" && $row_emp["CivilStatus"] == "") {
                                                $_++;
                                             } else {
                                                ${$row_emp["Sex"]."_".$row_emp["CivilStatus"]}++;
                                             }
                                             if ($row_emp["Sex"] != "") {
                                                ${$row_emp["Sex"]}++;
                                             } else {
                                                $NoGender++;
                                             }
                                          }
                                          include 'inc/inc_civil_status_by_gender.e2e.php';
                                       }
                                    ?>
                                 </div>
                                 <div class="row margin-top">
                                    <?php if (mysqli_num_rows($rsEmployees) > 0) { ?>
                                       <div class="col-xs-12">
                                          <div class="row margin-top" style="padding: 15px;">
                                             <div class="col-xs-4">
                                                <div class="row">
                                                   <div class="col-xs-3" style="background: #00477e; height: 20px;"></div>
                                                   <div class="col-xs-9">MALE</div>
                                                </div>   
                                             </div>
                                             <div class="col-xs-4">
                                                <div class="row">
                                                   <div class="col-xs-3" style="background: #a6001a; height: 20px;"></div>
                                                   <div class="col-xs-9">FEMALE</div>
                                                </div>   
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-12">
                                                <span class="label">
                                                   Total Number of Employees: 
                                                </span>
                                                <?php echo mysqli_num_rows($rsEmployees); ?>
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-6">
                                                <span class="label">
                                                   Male Employees: 
                                                </span>
                                                <?php echo $M; ?>
                                             </div>
                                             <div class="col-xs-6">
                                                <span class="label">
                                                   Female Employees: 
                                                </span>
                                                <?php echo $F; ?>
                                             </div>
                                          </div>
                                       </div>
                                    <?php }?>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- Modal -->
                  <div class="modal fade border0" id="prnModal" role="dialog">
                     <div class="modal-dialog border0" style="padding:0px;width:97%;height:92%;">
                        <div class="mypanel border0" style="height:100%;">
                           <div class="panel-top bgSilver">
                              <a href="#" data-toggle="tooltip" data-placement="top" title="Print Now" id="btnPRINTNOW">
                                 <i class="fa fa-print" aria-hidden="true"></i>
                              </a>
                              <label class="close" data-dismiss="modal">&times;</button>
                           </div>
                           <iframe id="rptContent" src="blank.e2e.php" class="iframes"></iframe>
                        </div>
                     </div>
                  </div>
            <?php

               } else {
                  /*
                     USER SIDE SCRN WELCOME
                  */
                  spacer(25);
            ?>
                  
                  <div class="row" style="margin:0px;">
                     <div class="col-sm-6" style="margin:0px;">
                        <?php
                           include 'conn.e2e.php';
                           $sql = "SELECT * FROM `employees` WHERE RefId = ".getvalue("hEmpRefId");
                           $rs = mysqli_query($conn,$sql);
                           if (mysqli_num_rows($rs) > 0)
                           {
                              $row = mysqli_fetch_assoc($rs)

                        ?>
                              <table>
                                 <tr>
                                    <td width="50%" align="center" valign="middle">
                                       <p>
                                       <?php
                                          if ($row['PicFilename'] != "") {
                                             if (file_exists(img($row['CompanyRefId']."/EmployeesPhoto/".$row['PicFilename']))) {
                                                echo '<img src="'.img($row['CompanyRefId']."/EmployeesPhoto/".$row['PicFilename']).'" style="width:150px;">';
                                             } else {
                                                echo '<img src="'.img("nopic.png").'" style="width:150px;">';
                                             }
                                          } else {
                                             echo '<img src="'.img("nopic.png").'" style="width:150px;">';
                                          }
                                       ?>
                                       </p>
                                       <p>
                                       <label
                                       class="btn-cls2-red"
                                       id="btnChangePW" name="btnChangePW">Change Password
                                       </button>
                                       </p>
                                    </td>
                                    <td width="50%">
                                       <?php
                                          $result = FindFirst('empinformation',"WHERE CompanyRefId = $CompanyId AND BranchRefId = $BranchId AND EmployeesRefId = ".$row["RefId"],"*");
                                          $info = array_merge($row,$result);
                                          $templ->doEmployeeInfo($info);
                                       ?>
                                    </td>
                                 </tr>
                              </table>   
                        <?php
                           }
                        ?>
                     </div>
                     <div class="col-xs-1"></div>
                     <div class="col-xs-4">
                        <div id="panelReminders">
                           <div class="row">
                              <div class="col-xs-12">
                              <div class="panel-top">PERSONAL INFORMATION SYSTEM (PIS)</div>
                                 <div class="panel-mid">
                                    <div class="row">
                                       <div class="col-xs-12">
                                          <button type="button"
                                               class="Menu btn-cls2-tree"
                                               pre="scrn"
                                               route="201File"
                                                  id="userPDS">
                                             <li><u>PERSONAL DATA SHEET (PDS)</u></li>
                                          </button>
                                       </div>
                                    </div>
                                    <div class="row margin-top">
                                       <div class="col-xs-12">
                                          <button type="button"
                                               class="Menu btn-cls2-tree"
                                               pre="scrn"
                                               route="EmpAttach" disabled>
                                             <li><u>201 FILE ATTACHMENTS</u></li>
                                          </button>
                                       </div>
                                    </div>
                                    <div class="row margin-top">
                                       <div class="col-xs-12">
                                          <button type="button"
                                               class="Menu btn-cls2-tree"
                                               pre="user"
                                               route="SERVICERECORD" disabled>
                                             <li><u>SERVICE RECORD</u></li>
                                          </button>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="panel-bottom"></div>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-12">
                              <div class="panel-top">ATTENDANCE MANAGEMENT SYSTEM (AMS)</div>
                                 <div class="panel-mid">
                                    <div class="row">
                                       <div class="col-xs-12">
                                          <button type="button" 
                                               class="Menu btn-cls2-tree"
                                               pre="ams"
                                               route="TrnDTR"
                                               id="userDTR" disabled>
                                          <li><u>DAILY TIME RECORD (DTR)</u></li>
                                       </button>
                                       </div>
                                    </div>
                                    <div class="row margin-top">
                                       <div class="col-xs-12">
                                          <button type="button"
                                               class="Menu btn-cls2-tree"
                                               pre="ams"
                                               route="LeaveCard" disabled>
                                             <li><u>LEAVE CARD</u></li>
                                          </button>
                                       </div>
                                    </div>
                                    <?php bar();?>
                                    <div class="row margin-top">
                                       <div class="col-xs-12">
                                          <button type="button"
                                               class="Menu btn-cls2-tree"
                                               pre="ams"
                                               route="ReqChangeShift" disabled>
                                             <li><u>REQUEST FOR CHANGE SHIFT</u></li>
                                          </button>
                                       </div>
                                    </div>
                                    <div class="row margin-top">
                                       <div class="col-xs-12">
                                          <button type="button"
                                               class="Menu btn-cls2-tree"
                                               pre="ams"
                                               route="ReqOvertime" disabled>
                                             <li><u>REQUEST FOR OVERTIME</u></li>
                                          </button>
                                       </div>
                                    </div>
                                    <div class="row margin-top">
                                       <div class="col-xs-12">
                                          <button type="button"
                                               class="Menu btn-cls2-tree"
                                               pre="ams"
                                               route="ReqForceLeave" disabled>
                                             <li><u>REQUEST FOR CANCELLATION OF LEAVE</u></li>
                                          </button>
                                       </div>
                                    </div>
                                    <?php bar();?>
                                    <div class="row margin-top">
                                       <div class="col-xs-12">
                                          <button type="button"
                                               class="Menu btn-cls2-tree"
                                               pre="ams"
                                               route="AvailLeave" disabled>
                                             <li><u>AVAILMENT OF LEAVE</u></li>
                                          </button>
                                       </div>
                                    </div>
                                    <div class="row margin-top">
                                       <div class="col-xs-12">
                                          <button type="button"
                                               class="Menu btn-cls2-tree"
                                               pre="ams"
                                               route="AvailCTO" disabled>
                                             <li><u>AVAILMENT OF CTO</u></li>
                                          </button>
                                       </div>
                                    </div>
                                    <div class="row margin-top">
                                       <div class="col-xs-12">
                                          <button type="button"
                                               class="Menu btn-cls2-tree"
                                               pre="ams"
                                               route="AvailAuthority" disabled>
                                             <li><u>AVAILMENT OF OFFICE AUTHORITY</u></li>
                                          </button>
                                       </div>
                                    </div>
                                    <div class="row margin-top">
                                       <div class="col-xs-12">
                                          <button type="button"
                                               class="Menu btn-cls2-tree"
                                               pre="ams"
                                               route="AvailLeaveMonitization" disabled>
                                             <li><u>AVAILMENT OF LEAVE MONETIZATION</u></li>
                                          </button>
                                       </div>
                                    </div>

                                 </div>
                                 <div class="panel-bottom"></div>
                              </div>
                           </div>
                        </div>
                        <?php if ($settings["show.MyRequest"]) { ?>
                           <div class="mypanel" id="panelRequest">
                              <div class="row margin-top" style="margin:0px;">
                                 <div class="col-sm-12">
                                    <div class="panel-top" id="myrequest">My Request(s)</div>
                                    <div class="panel-mid" id="myrequestView">
                                       <div class="row" style="margin:0px;">
                                          <div class="col-xs-12">
                                             <?php
                                                include_once 'conn.e2e.php';
                                                $sql = "SELECT * FROM updates201 WHERE EmployeesRefId = ".getvalue("hEmpRefId"). " AND Status = 'P'";
                                                $rs = mysqli_query($conn,$sql);
                                                if (mysqli_num_rows($rs) > 0) {
                                             ?>
                                                <div class="row" style="border-bottom: 1px solid black;">
                                                   <div class="col-xs-3">
                                                         <label>CHANGE IN</label>
                                                   </div>
                                                   <div class="col-xs-3">
                                                         <label>REQUEST FROM</label>
                                                   </div>
                                                   <div class="col-xs-3">
                                                         <label>REQUEST TO</label>
                                                   </div>

                                                   <div class="col-xs-3">
                                                         <label>ACTION</label>
                                                   </div>
                                                </div>
                                             <?php
                                                   while ($row = mysqli_fetch_assoc($rs)) {

                                                      if ($row["Status"] == "P") {
                                                         $Status = "PENDING";
                                                      }
                                             ?>
                                                <div class="row margin-top">
                                                   <div class="col-xs-3">
                                                         <label><?php echo $row["FieldsEdit"]; ?></label>
                                                   </div>
                                                   <div class="col-xs-3">
                                                         <label><?php echo $row["OldValue"]; ?></label>
                                                   </div>
                                                   <div class="col-xs-3">
                                                         <label><?php echo $row["NewValue"]; ?></label>
                                                   </div>
                                                   <div class="col-xs-3">
                                                      <label id="cancel_<?php echo $row["RefId"]; ?>">CANCEL</label>
                                                   </div>
                                                </div>
                                             <?php
                                                   }
                                                } else {
                                                   echo '<p>
                                                            <a href="javascript:void(0);">
                                                               <h5>No Current Request</h5>
                                                            </a>
                                                         </p>
                                                      ';
                                                }
                                             ?>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="panel-bottom"></div>
                                 </div>
                              </div>
                           </div>
                        <?php } ?>
                        <div class="mypanel margin-top" id="panelChangePW" style="display:none">
                           <div class="panel-top">Change Password</div>
                           <div class="panel-mid">
                              <div class="row">
                                 <div class="col-xs-6" style="margin-left: 20px;">
                                    <div class="row">
                                       <div class="form-group">
                                          <label>Current Password:</label>
                                          <input type="password" name="currentToken" id="currentToken" class="form-input">
                                       </div>
                                    </div>
                                    <?php entryAlert("cuPW","Wrong Current Password !!!"); ?>
                                    <div class="row">
                                       <div class="form-group">
                                          <label>New Password:</label>
                                          <input type="password" name="newToken" id="newToken" class="form-input">
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="form-group">
                                          <label>Re-Type Password:</label>
                                          <input type="password" name="reToken" id="reToken" class="form-input">
                                       </div>
                                    </div>
                                    <?php entryAlert("rePW","Mismacthed of New Password !!!"); ?>
                                    <div class="row">
                                       <div class="form-group">
                                          <button
                                          class="btn-cls2-sea"
                                          id="btnChangeNow" name="btnChangeNow">Change Now
                                          </button>
                                          <button
                                          class="btn-cls2-red"
                                          id="btnChangeCancel" name="btnChangeCancel">Cancel
                                          </button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="panel-bottom"></div>
                        </div>
                     </div>   
                  </div>
                  
            <?php
               }
               footer();
               include "varHidden.e2e.php";
               doHidden("hRptFile","DashboardRpt","");
            ?>
         </div>
      </form>
      <script language="JavaScript" src="<?php echo jsCtrl("ctrl_Dashboard"); ?>"></script>
   </body>
</html>


