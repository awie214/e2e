<?php
   $moduleContent = file_get_contents(json.'reports.json');
   $module = json_decode($moduleContent, true); 
   $objs = $module["Fields"];
   $label = $module["Label"];
   $inputType = $module["InputType"];
   $class = $module["Class"];
   $defvalue = $module["DefaultValue"];
   $css = $module["css"];
   $count = count($objs);
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"];?>
      <script language="JavaScript">
         $(document).ready(function () {
            $("#bAdd").click(function () {
               $("#DataEntry").modal();
            });

         }); 
      </script>
      <style>
         .grid_Table {
            width: 100%;
            background: #fff;
            border-collapse: collapse;
            border: 1px solid silver;
         }
         .grid_Table > thead {
            height: 1cm;
            color : #fff;
            background: #5b5e6a;
            font-weight: 600;
         }
         .grid_Table > tbody tr {
            height: .6cm;
         }
         .grid_Table > tbody tr:hover {
            background:#e2e2e2;
            cursor: pointer;
         }
         .grid_Table > tbody td {
            padding:2px;
         }
         .trnButton {
            min-width:60px;
            border:0px solid #373538;
            border-radius: 5px;
            background-color: #47484a;
            color:#fff;
            font-family: verdana;
            font-size: 9pt;
            font-weight: 600;
            height:.65cm;
         } 

      </style>

   </head>
   <body>
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"pis"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar($module["Title"]); ?>

            <?php spacer(5) ?>
            <div class="row">
               <div class="col-sm-4">
                  <button type="button" class="trnButton" style="width:70px;" name="bAdd" id="bAdd">Insert</button>
               </div>
               <div class="col-sm-4">
               </div>
               <div class="col-sm-4 txt-right">
                  <input type="text" class="form-input" style="width:150px;" placeholder="Search Here">
                  <a href="javascript:void(0);">
                     <i class="fa fa-search" aria-hidden="true"></i>
                  </a>
               </div>
            </div>
            <?php spacer(5) ?>
            <div class="row" style="max-height:400px;overflow:auto;">
               <table class="grid_Table" border=1>
                  <thead> 
                     <tr>
                        <th class="txt-center">Action</th>
                        <?php 
                           for ($i=0;$i<count($objs);$i++) {
                              echo '<th class="txt-center">'.$label[$i].'</th>';
                           }
                        ?>
                     </tr>
                     </thead>
                     </tbody>
                     <?php
                        $rs = SelectEach($module["Table"],'');
                        if ($rs) {
                           $action = $module["Action"];
                           while ($row = mysqli_fetch_assoc($rs)) {
                              echo '<tr>';
                              echo
                              '<td align="center">';
                                 if (isset($action[0]) && $action[0]) {
                                    echo
                                    '<a style="text-decoration:none;cursor:pointer;margin-right:10px" title="View this Record">
                                       <img src="'.img("view.png").'" onclick="">
                                    </a>';
                                 }
                                 if (isset($action[1]) && $action[1]) {
                                    echo
                                    '<a style="text-decoration:none;cursor:pointer;margin-right:10px" title="Modify this Record">
                                       <img src="'.img("edit.png").'" onclick="">
                                    </a>';
                                 }
                                 if (isset($action[2]) && $action[2]) {
                                    echo
                                    '<a style="text-decoration:none;cursor:pointer" onclick="deleteRecord('.$row["RefId"].');"
                                       title="Delete This Record">
                                       <img src="'.img("delete.png").'">
                                    </a>';
                                 }
                                 if (isset($action[3]) && $action[3]) {
                                    echo
                                    '<input type="radio" name="obj" id="idx_'.$refid.'" onclick="selectMe('.$refid.');">';
                                 }
                              echo
                              '</td>';

                              for ($i=0;$i<count($objs);$i++) {
                                 if ($css[$i] != "") {
                                    $cssStyle = 'style="'.$css[$i].'";';
                                 } else {
                                    $cssStyle = "";
                                 }

                                 echo '<td '.$cssStyle.'>'.$row[$objs[$i]].'</td>';
                              }
                              echo '</tr>';
                           }
                        }
                     ?>
                     </tbody>
               </table>
               <div class="txt-center">
                  <ul class = "pagination">
                     <li><a href = "#">&laquo;</a></li>
                     <li><a href = "#">1</a></li>
                     <li><a href = "#">2</a></li>
                     <li><a href = "#">3</a></li>
                     <li><a href = "#">4</a></li>
                     <li><a href = "#">5</a></li>
                     <li><a href = "#">&raquo;</a></li>
                  </ul>
               </div>   
            </div>
            

            <div class="modal fade border0" id="DataEntry" role="dialog">
               <div class="modal-dialog border0">
                  <div class="mypanel margin-top">
                     <div class="panel-top">                        
                     </div>
                     <div class="panel-mid">
                        <div class="row">
                           <div class="col-sm-12">
                              <?php 
                                 for ($i=0;$i<count($objs);$i++) {
                                    echo  
                                    '<div>
                                       <div class="form-group">
                                          <label class="control-label" for="'.$objs[$i].'">'.$label[$i].'</label><br>'."\n";
                                          switch ($inputType[$i]) {
                                             case "Text";
                                                echo '<input type="Text" class="'.$class[$i].'" value="'.$defvalue[$i].'" name="'.$objs[$i].'" id="'.$objs[$i].'">';
                                             break;
                                             case "Drop";
                                                echo 
                                                '<select class="'.$class[$i].'" name="'.$objs[$i].'" id="'.$objs[$i].'">';
                                                   $items = explode(",",$defvalue[$i]);
                                                   foreach ($items as $key) {
                                                      $key_Arr = explode("|",$key);
                                                      echo '<option value="'.$key_Arr[0].'">'.$key_Arr[1].'</option>';
                                                   }
                                                echo   
                                                '</select>';
                                             break;
                                          } 
                                       echo
                                       '</div>
                                    </div>'."\n";
                                 }
                              ?>
                           </div>
                        </div>
                     </div>
                     <div class="panel-bottom">
                        <div class="row">
                           <div class="col-sm-4">
                              <button type="button" class="trnButton" style="width:70px;" name="bAdd" id="bAdd">Save</button>
                              <button type="button" class="trnButton" style="width:70px;" name="bAdd" id="bAdd">Cancel</button>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <?php
               footer();
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
   </body>
</html>



