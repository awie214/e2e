<?php
   $LastName = $FirstName = $MiddleName = $Street = $City = $Province = $ZipCode = $FathersName = "";
   $MothersName = $ContactNo = $EmergencyContactNo = $BirthDate = $EmailAdd = $IDNo = $EmergencyContactPerson = $Gender = "";
   $GuardianName = $GuardianContactNo = $picFileName = $NickName = $BirthPlace = $Height = $Weight = $Complexion = $HairColor = "";
   $Religion = $Citizenship = $BirthOrder = $ExtName = $NickName = $BirthDate = "";
   $ContactNo = $EmailAdd = "";
   $msg = $search_LASTNAME = $search_FIRSTNAME = $search_STUDENTREFID = $search_GRDLVL = "";
   $disabled = "disabled";
   $user = getvalue("hUser");
   $Employees = FindFirst("employees","WHERE RefId = ".getvalue("hEmpRefId"),"*");
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script type="text/javascript" src="<?php echo jsPath("jsSHAver2/src/sha"); ?>"></script>
      <script type="text/javascript" src="<?php echo jsPath("js7_login"); ?>"></script>
      <script type="text/javascript" src="<?php echo jsCtrl("ctrl_Users"); ?>"></script>
      <script type="text/javascript">
         <?php
            if (!$Employees) {
               echo 'console.log("NO REFID ASSIGNED");';
            } 
         ?>
      </script>
      
   </head>
   <body>
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"pis"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php
               doTitleBar($paramTitle);
            ?>
            <div class="container-fluid margin-top10">
                     <div class="row">
                        <div class="col-xs-10">
                           <div class="row">
                              <div class="col-xs-2 text-center">
                                 <div class="row">
                                    <?php
                                       
                                       $fileExist = file_exists(path."images/EmployeesPhoto/".$Employees["PicFilename"]);
                                       if ($fileExist) {
                                          $userPic = path."images/EmployeesPhoto/".$Employees["PicFilename"];
                                       } else {
                                          $userPic = img("nopic.png");
                                       }
                                    ?>
                                    <span id="Pic">
                                       <img src="<?php echo $userPic; ?>"
                                          style="margin-bottom:3px;border:1px solid #aaa;width:1.2in;height:1.5in;"/><br>
                                       <button type="button"
                                               class="btn-cls4-sea trnbtn"
                                               id="btnUPLOADPIC" name="btnUPLOADPIC"
                                               title="Upload Picture"
                                               >
                                                <i class="fa fa-upload"></i>&nbsp;&nbsp;Upload Picture
                                       </button>
                                    </span>
                                 </div>
                              </div>
                              <div class="col-xs-8">
                                 <div class="row">
                                    <div class="col-xs-4"></div>
                                    <div class="col-xs-8 txt-right">
                                       <!--
                                       <span id="btnViewMode">
                                          <?php createButton("EDIT",
                                                             "btnEDIT",
                                                             "btn-cls4-sea trnbtn",
                                                             "fa-edit",
                                                             "")?>
                                          <button type="button"
                                                  class="btn-cls4-red trnbtn"
                                                  id="btnEXIT" name="btnEXIT">
                                             <i class="fa fa-undo" aria-hidden="true"></i>&nbsp;&nbsp;EXIT
                                          </button>
                                       </span>
                                       -->
                                       <span id="btnNewMode">
                                          <button type="button" class="btn-cls4-sea trnbtn"
                                                  name="btnSAVEu" id="btnSAVEu">
                                             <i class="fa fa-file" aria-hidden="true"></i>&nbsp;&nbsp;SAVE
                                          </button>
                                          <button type="button" class="btn-cls4-red trnbtn"
                                                  name="btnCANCEL" id="btnCANCEL">
                                             <i class="fa fa-undo" aria-hidden="true"></i>&nbsp;&nbsp;CANCEL
                                          </button>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="row margin-top20">
                                    <div class="col-xs-4">
                                    </div>
                                    <?php if ($GLOBALS["UserCode"]!="COMPEMP" && $mode == "ADD") { ?>
                                    <div class="col-xs-4">
                                       <button type="button" class="btn-cls4-sea trnbtn"
                                               name="btnEmpLkUp" id="btnEmpLkUp">
                                             Employees Look Up
                                       </button>
                                    </div>
                                    <?php } ?>
                                 </div>
                                 <div  >
                                    <div class="row margin-top">
                                       <div class="col-xs-4 text-right">
                                          <label>Employee RefId:</label>
                                       </div>
                                       <div class="col-xs-2">
                                          <div class="form-disp" id="empRefId">&nbsp;</div>
                                       </div>
                                    </div>
                                    <div class="row margin-top">
                                       <div class="col-xs-4 text-right">
                                          <label>Last Name :</label>
                                       </div>
                                       <div class="col-xs-8">
                                          <div class="form-disp" id="lastName">&nbsp;</div>
                                       </div>
                                    </div>
                                    <div class="row margin-top">
                                       <div class="col-xs-4 text-right">
                                          <label for="firstName">First Name :</label>
                                       </div>
                                       <div class="col-xs-8">
                                          <div class="form-disp" id="firstName">&nbsp;</div>
                                       </div>
                                    </div>
                                    <div class="row margin-top">
                                       <div class="col-xs-4 text-right">
                                          <label for="middleName">Middle Name :</label>
                                       </div>
                                       <div class="col-xs-8">
                                          <div class="form-disp" id="middleName">&nbsp;</div>
                                       </div>
                                    </div>
                                    <div class="row margin-top">
                                       <div class="col-xs-4 text-right">
                                          <label for="NickName">Ext. Name :</label>
                                       </div>
                                       <div class="col-xs-8">
                                          <div class="form-disp" id="extName">&nbsp;</div>
                                       </div>
                                    </div>
                                    <div class="row margin-top">
                                       <div class="col-xs-4 text-right">
                                          <label for="NickName">Nick Name :</label>
                                       </div>
                                       <div class="col-xs-8">
                                          <div class="form-disp" id="nickName">&nbsp;</div>
                                       </div>
                                    </div>
                                    <div class="row margin-top">
                                       <div class="col-xs-4 text-right">
                                          <label for="birthDate">Date of Birth :</label>
                                       </div>
                                       <div class="col-xs-8">
                                          <div class="form-disp" id="birthDate">&nbsp;</div>
                                       </div>
                                    </div>
                                    <div class="row margin-top">
                                       <div class="col-xs-4 text-right">
                                          <label for="NickName">Contact No. :</label>
                                       </div>
                                       <div class="col-xs-8">
                                          <div class="form-disp" id="contactNo">&nbsp;</div>
                                       </div>
                                    </div>
                                    <div class="row margin-top">
                                       <div class="col-xs-4 text-right">
                                          <label for="NickName">Email Address :</label>
                                       </div>
                                       <div class="col-xs-8">
                                          <div class="form-disp" id="emailAdd">&nbsp;</div>
                                       </div>
                                    </div>
                                    <div class="row margin-top">
                                       <div class="col-xs-4 text-right">
                                          <label>Gender :</label>
                                       </div>
                                       <div class="col-xs-8">
                                          <div class="form-disp" id="Gender">&nbsp;</div>
                                       </div>
                                    </div>
                                 </div>
                                 <?php bar(); ?>

                                 <div id="EntryScrn">
                                    <input type="hidden" class="saveFields--" value="" name="sint_EmployeesRefId">
                                    <div class="row">
                                       <div class="col-xs-4 text-right">
                                          <label for="userName"><span><b>**</b></span>&nbsp;USER NAME :</label>
                                       </div>
                                       <div class="col-xs-8">
                                          <input type="text"
                                                 name="char_UserName"
                                                 id="userName"
                                                 class="form-input saveFields--"
                                                 value=""
                                                 <?php echo $disabled; ?>
                                          >
                                       </div>
                                    </div>
                                    <div class="row margin-top">
                                       <div class="col-xs-4 text-right">
                                          <label for="NickName"><span><b>**</b></span>&nbsp;PASSWORD :</label>
                                       </div>
                                       <div class="col-xs-8">
                                          <input type="password" name="char_Password1"
                                             maxlength=15
                                             id="Password"
                                             placeholder="Password"
                                             class="form-input" value="" <?php echo $disabled; ?>
                                          >
                                       </div>
                                    </div>
                                    <div class="row margin-top">
                                       <div class="col-xs-4 text-right">
                                          <label><span><b>**</b></span>&nbsp;RETYPE PASSWORD :</label>
                                       </div>
                                       <div class="col-xs-8">
                                          <?php
                                             createForm("password",
                                             "char_Password2",
                                             "Password2","",'maxlength=15 placeholder="Confirm Password" ',"","");
                                          ?>
                                          <?php
                                          if ($mode == "VIEW") {
                                          ?>
                                             <br>
                                             <button type="button" style="margin-top:5px"
                                                     class="btn-cls4-red trnbtn"
                                                     id="btnCHANGEPW" name="btnCHANGEPW"
                                                     title="Change Password">
                                                   <i class="fa fa-lock"></i>&nbsp;&nbsp;Change Password
                                             </button>
                                          <?php } ?>
                                       </div>
                                       <input type="hidden" name="char_hNewReToken" class="HiddenEntry saveFields--" value="">
                                    </div>
                                    <div class="row margin-top">
                                       <div class="col-xs-4 text-right">
                                          <label><span><b>**</b></span>&nbsp;System Group :</label>
                                       </div>
                                       <div class="col-xs-8">
                                          <?php
                                             createSelect("SysGroup",
                                                          "sint_SysGroupRefId",
                                                          "SysGroup",100,"Name","Select System Group",$disabled);
                                          ?>
                                       </div>
                                    </div>
                                    <div class="row margin-top">
                                       <div class="col-xs-12 text-center" style="color:red">
                                          <b>NOTE:</b>
                                          Password should be atleast 8 alphanumeric characters
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-xs-2"></div>
                           </div>
                        </div>
                        <div class="col-xs-2" style="margin:0;">
                           <span class="float-r" style="color:red;font-weight:600;"><?php echo $mode; ?> MODE&nbsp;&nbsp;</span>
                        </div>
                     </div>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="changePW" role="dialog">
               <div class="modal-dialog">
                  <div class="mypanel">
                     <div class="panel-top">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4>Change Password</h4>
                     </div>
                     <div class="panel-mid-litebg">
                        <div>
                           <input type="password" class="form-input" id="txtPW" name="txtPW"
                           placeholder="Current Password" maxlength=15>
                        </div>
                        <div class="margin-top">
                           <input type="password" class="form-input" id="txtnPW" name="txtnPW"
                           placeholder="New Password" maxlength=15>
                        </div>
                        <div class="txt-center padd10">
                           <button type="button" class="btn-cls4-sea" id="btnChangePW" onclick="changeNow();">
                              <i class="fa fa-unlock" aria-hidden="true"></i>&nbsp;&nbsp;Change It Now
                           </button>
                           <button type="button" class="btn-cls4-red" data-dismiss="modal">
                              <i class="fa fa-times" aria-hidden="true"></i>&nbsp;&nbsp;Close
                           </button>&nbsp;
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <input type="hidden" name="hNewToken" value="">
            <input type="hidden" name="sint_EmployeesRefId" id="empRefId" class="form-input HiddenEntry">
            <?php
               modalEmpLkUp();
               footer();
               $table = "sysuser";
               //$refid = $_SESSION['sess_user_refid'];
               $refid = getvalue("hEmpRefId");
               doHidden("uplClose",getvalue("uplClose"),"");
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
   </body>
</html>


