<!DOCTYPE html>
<html>
   <head>
      <?php 
         include 'conn.e2e.php';
         include_once "constant.e2e.php";
         include_once pathClass.'0620functions.e2e.php';
         include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl(""); ?>"></script>
      <style>
         td {vertical-align:top;}
         .tableHead {
            text-align:center;
            vertical-align::middle;
            line-height: 40px;
         }
         tr {text-align:center;}
      </style>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <div class="row">
            <div class="col-xs-6">
               <span class="label">Agency Name: E2E SOLUTION</span>
            </div>
            <div class="col-xs-6 txt-right">
               <span id="TimeDate">&nbsp;</span>
            </div>
         </div>
         <?php spacer(30);?>
         <div class="row">
            <div class="col-xs-3">
               <span class="label">Employee Name:</span>
            </div>
            <div class="col-xs-3">
               <span class="label"></span>
            </div>
            <div class="col-xs-3">
               <span class="label">Status:</span>
            </div>
            <div class="col-xs-3">
               <span class="label"></span>
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-xs-3">
               <span class="label">Agency ID:</span>
            </div>
            <div class="col-xs-3">
               <span class="label"></span>
            </div>
            <div class="col-xs-3">
               <span class="label">Schedule:</span>
            </div>
            <div class="col-xs-3">
               <span class="label"></span>
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-xs-3">
               <span class="label">Position:</span>
            </div>
            <div class="col-xs-3">
               <span class="label"></span>
            </div>
            <div class="col-xs-3">
               <span class="label">Date Period:</span>
            </div>
            <div class="col-xs-3">
               <span class="label"></span>
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-xs-3">
               <span class="label">Division:</span>
            </div>
            <div class="col-xs-3">
               <span class="label"></span>
            </div>
         </div>
         <?php spacer(30);?>
         <table border=1 width="100%">
            <tr class="tableHead">
               <td rowspan="1" colspan="2">Monthly</td>
               <td rowspan="2">IN 1</td>
               <td rowspan="2">OUT 1</td>
               <td rowspan="2">IN 2</td>
               <td rowspan="2">OUT 2</td>
               <td rowspan="2">O.T IN</td>
               <td rowspan="2">O.T OUT</td>
               <td rowspan="2">Total Hours</td>
               <td rowspan="2">Late</td>
               <td rowspan="2">Undertime</td>
               <td rowspan="2">Status/Remarks</td>
               
            </tr>
            <tr class="tableHead">
               <td>Date</td>
               <td>Day</td>
            </tr>
            <?php 
               for ($i=1;$i<=31;$i++) {
               echo '
                  <tr>
                     <td>'.$i.'</td>
                     <td></td>
                     <td></td>
                     <td></td>
                     <td></td>
                     <td></td>
                     <td></td>
                     <td></td>
                     <td></td>
                     <td></td>
                     <td></td>
                     <td></td>
                  </tr>
               ';
               }
               ?>
         </table>
         <?php spacer(20);?>
         <div class="row">
            <div class="col-xs-3">
               <span class="label">Total Hours:</span>
            </div>
            <div class="col-xs-3">
               <span class="label">Total O.T:</span>
            </div>
            <div class="col-xs-3">
               <span class="label">Total Hours:</span>
            </div>
            <div class="col-xs-3">
               <span class="label">Total Day Works:</span>
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-xs-3">
               <span class="label">Absent:</span>
            </div>
            <div class="col-xs-3">
               <span class="label">Used Leave:</span>
            </div>
            <div class="col-xs-3">
               <span class="label">Absent:</span>
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-xs-3">
               <span class="label">Total LT:</span>
            </div>
            <div class="col-xs-3">
               <span class="label">Used Office Authority:</span>
            </div>
            <div class="col-xs-3">
               <span class="label">Total LT:</span>
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-xs-3">
               <span class="label">Total UT:</span>
            </div>
            <div class="col-xs-3">
               <span class="label">Used CTO:</span>
            </div>
            <div class="col-xs-3">
               <span class="label">Total UT:</span>
            </div>
         </div>
         <?php spacer(20);?>
         <div class="row txt-center">
            <p>I certify that the entries on this record, which were made by myself daily at the time of arrival and departure from office are true and correct.</p>
         </div>
         <div class="row txt-center">
            <div class="col-xs-6">
               ___________________________
            </div>
            <div class="col-xs-6">
               <span class="label"><u>Juan Dela Cruz</u></span>
            </div>
         </div>
         <div class="row txt-center">
            <div class="col-xs-6">
               <span class="label">Employee Signature</span>
            </div>
            <div class="col-xs-6">
               <span class="label">Employee Name</span>
            </div>
         </div>
         <div class="row txt-center">
            <div class="col-xs-6"></div>
            <div class="col-xs-6">
               <span class="label">Position</span>
            </div>
         </div>
         <?php spacer(30);?>
         <div class="row txt-center" style="border:1px solid black;padding:5px;">
            <span class="label">
               Reminder: Failure to return within 5 days together with the required official documents will mean withholding salaries of the employee's concerned.
            </span>
         </div>
      </div>
   </body>
</html>