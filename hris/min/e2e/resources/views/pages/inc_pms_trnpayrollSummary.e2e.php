<div class="mypanel">
   <div class="row">
      <div class="col-xs-9" style="padding:10px;">
         <div class="row" style="border:1px solid black;padding:5px;">
            <div class="col-xs-12">
               <div class="row margin-top">
                  <div class="col-xs-3"></div>
                  <div class="col-xs-3">
                     <label>Actual</label>
                  </div>
                  <div class="col-xs-3">
                     <label>Adjustment</label>
                  </div>
                  <div class="col-xs-3">
                     <label>Total</label>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-3">
                     <label>Basic Pay</label>
                  </div>
                  <div class="col-xs-3">
                     <input type="text" name="ActualBasicPay" class="form-input number--">
                  </div>
                  <div class="col-xs-3">
                     <input type="text" name="AdjustmentBasicPay" class="form-input number--">
                  </div>
                  <div class="col-xs-3">
                     <input type="text" name="TotalBasicPay" class="form-input number--">
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-3">
                     <label>Absences</label>
                  </div>
                  <div class="col-xs-3">
                     <input type="text" name="ActualAbsences" class="form-input number--">
                  </div>
                  <div class="col-xs-3">
                     <input type="text" name="AdjustmentAbsences" class="form-input number--">
                  </div>
                  <div class="col-xs-3">
                     <input type="text" name="TotalAbsences" class="form-input number--">
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-3">
                     <label>Tardiness</label>
                  </div>
                  <div class="col-xs-3">
                     <input type="text" name="ActualTardiness" class="form-input number--">
                  </div>
                  <div class="col-xs-3">
                     <input type="text" name="AdjustmentTardiness" class="form-input number--">
                  </div>
                  <div class="col-xs-3">
                     <input type="text" name="TotalTardiness" class="form-input number--">
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-3">
                     <label>Undertime</label>
                  </div>
                  <div class="col-xs-3">
                     <input type="text" name="ActualUndertime" class="form-input number--">
                  </div>
                  <div class="col-xs-3">
                     <input type="text" name="AdjustmentUndertime" class="form-input number--">
                  </div>
                  <div class="col-xs-3">
                     <input type="text" name="TotalUndertime" class="form-input number--">
                  </div>
               </div>
               
               
               
               <div class="row margin-top">
                  <div class="col-xs-3">
                  </div>
                  <div class="col-xs-3">
                  </div>
                  <div class="col-xs-3 txt-center">
                     <label>Basic Net Pay</label>
                  </div>
                  <div class="col-xs-3">
                     <input type="text" name="BasicNetPay" class="form-input number--">
                  </div>
               </div>
               
               
               <div class="row margin-top">
                  <div class="col-xs-3">
                     <label>Overtime Pay</label>
                  </div>
                  <div class="col-xs-3">
                     <input type="text" name="ActualOvertimePay" class="form-input number--">
                  </div>
                  <div class="col-xs-3">
                     <input type="text" name="AdjustmentOvertimePay" class="form-input number--">
                  </div>
                  <div class="col-xs-3">
                     <input type="text" name="TotalOvertimePay" class="form-input number--">
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-3">
                     <label>Non-Taxable Allowance</label>
                  </div>
                  <div class="col-xs-3">
                     <input type="text" name="ActualNonTaxableAllowance" class="form-input number--">
                  </div>
                  <div class="col-xs-3">
                     <input type="text" name="AdjustmentNonTaxableAllowance" class="form-input number--">
                  </div>
                  <div class="col-xs-3">
                     <input type="text" name="TotalNonTaxableAllowance" class="form-input number--">
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-3">
                     <label>Taxable Allowance</label>
                  </div>
                  <div class="col-xs-3">
                     <input type="text" name="ActualTaxableAllowance" class="form-input number--">
                  </div>
                  <div class="col-xs-3">
                     <input type="text" name="AdjustmentTaxableAllowance" class="form-input number--">
                  </div>
                  <div class="col-xs-3">
                     <input type="text" name="TotalTaxableAllowance" class="form-input number--">
                  </div>
               </div>
            </div>
         </div>
         <?php spacer(30);?>
         <div class="row margin-top" style="border:1px solid black;padding:5px;">
            <div class="col-xs-12">
               <div class="row margin-top">
                  <div class="col-xs-3">
                     <label>Total Contributions</label>
                  </div>
                  <div class="col-xs-3">
                     <input type="text" name="ActualTotalContributions" class="form-input number--">
                  </div>
                  <div class="col-xs-3">
                     <input type="text" name="AdjustmentTotalContributions" class="form-input number--">
                  </div>
                  <div class="col-xs-3">
                     <input type="text" name="TotalTotalContributions" class="form-input number--">
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-3">
                     <label>Total Loans</label>
                  </div>
                  <div class="col-xs-3">
                     <input type="text" name="ActualTotalLoans" class="form-input number--">
                  </div>
                  <div class="col-xs-3">
                     <input type="text"  name="AdjustmentTotalLoans" class="form-input number--">
                  </div>
                  <div class="col-xs-3">
                     <input type="text"  name="TotalLoans" class="form-input number--">
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-3">
                     <label>Total Other Deductions</label>
                  </div>
                  <div class="col-xs-3">
                     <input type="text" name="ActualTotalOtherDeductions" class="form-input number--">
                  </div>
                  <div class="col-xs-3">
                     <input type="text" name="AdjustmentTotalOtherDeductions" class="form-input number--">
                  </div>
                  <div class="col-xs-3">
                     <input type="text" name="TotalOtherDeductions" class="form-input number--">
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-3"></div>
                  <div class="col-xs-3"></div>
                  <div class="col-xs-3 txt-center">
                     <label>Total Deductions</label>
                  </div>
                  <div class="col-xs-3">
                     <input type="text" name="Total Deductions" class="form-input number--">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col-xs-3">
         <div class="row margin-top">
            <div class="col-xs-6">
               <label>Gross Pay</label>
            </div>
            <div class="col-xs-6">
               <input type="text" name="GrossPay" class="form-input number--">
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-xs-6">
               <label>Gross Taxable Pay</label>
            </div>
            <div class="col-xs-6">
               <input type="text" name="GrossTaxablePay" class="form-input number--">
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-xs-6">
               <label>Net Pay</label>
            </div>
            <div class="col-xs-6">
               <input type="text" name="NetPay" class="form-input number--">
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-xs-6"></div>
            <div class="col-xs-6 txt-right">
               <input type="checkbox" name="chkHold">&nbsp;Hold
            </div>
         </div>
      </div>
   </div>
</div>