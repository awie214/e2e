$(document).ready(function() {
   addParam = "";
   $("#Report1").click(function(){
      addParam += "&hRptFile=spmsReportList";
      addParam += "&paramTitle=Report1";
      addParam += "&hRptScrn=spmsReport1";
      gotoscrn("spmsReports",addParam);
   });
  $("#Report2").click(function(){
      addParam += "&hRptFile=spmsReportList";
      addParam += "&paramTitle=Report2";
      addParam += "&hRptScrn=spmsReport1";
      gotoscrn("spmsReports",addParam);
   });
   $("#Report3").click(function(){
      addParam += "&hRptFile=spmsReportList";
      addParam += "&paramTitle=Report3";
      addParam += "&hRptScrn=spmsReport1";
      gotoscrn("spmsReports",addParam);
   });
   $("#Report4").click(function(){
      addParam += "&hRptFile=spmsReportList";
      addParam += "&paramTitle=Report4";
      addParam += "&hRptScrn=spmsReport1";
      gotoscrn("spmsReports",addParam);
   });

   $("#closeRpt").click(function(){
      addParam += "&paramTitle=Reports Type";
      gotoscrn("spmsReports",addParam);
   });


   $("#btnGENERATE").click(function(){
      //$("#rptCriteria").hide();
      //$("#btnShowCrit").prop("disabled",false);
      //$("#btnHideCrit").prop("disabled",true);
      //genReport("rptCriteria");
      $("#rptContent").attr("src","blank.e2e.php");
      var rptFile = $("#hRptFile").val();
      var url = "ReportCaller.e2e.php?file=" + rptFile + "&";
      url += getRPTCriteria("rptCriteria");
      $("#prnModal").modal();
      $("#rptContent").attr("src",url);
   });
   /*$("#btnShowCrit").click(function(){
      $("#rptCriteria").show();
      $("#btnShowCrit").prop("disabled",true);
      $("#btnHideCrit").prop("disabled",false);
   });
   $("#btnHideCrit").click(function(){
      $("#rptCriteria").hide();
      $("#btnHideCrit").prop("disabled",true);
      $("#btnShowCrit").prop("disabled",false);
   });*/
   $("#btnPRINT").click(function(){

   });
   $("#btnEXIT").click(function(){
      gotoscrn("spmsReports",addParam);
   });
   $("#checkAll").change(function(){
      if ($(this).is(':checked')) {
         $(".showCol--").prop("checked",true);
      } else {
         $(".showCol--").prop("checked",false);
      }
      $("[name='chkEmpName']").prop("checked",true);
   });


});
function selectMe(emprefid){
   $.post("EmpQuery.e2e.php",
   {
      emprefid:emprefid
   },
   function(data,status) {
      if (status == "error")return false;
      if (status == "success") {
         var data = JSON.parse(data);
         try
         {
         //$('[name="txtRefId"]').val(data.EmpRefId);
         $('[name="txtLName"]').val(data.LastName);
         $('[name="txtFName"]').val(data.FirstName);
         $('[name="txtMidName"]').val(data.MiddleName);
         $("#modalEmpLookUp").modal("hide");
         }
         catch (e)
         {
            if (e instanceof SyntaxError) {
                alert(e.message);
            }
         }
      }
   });

}

//$(document).ready(function(){
//  $('#btnGENERATE').click(function(){
//    alert($('#drpCvlStats :selected').text());
//  });
//});

function closeSCRN(scrntype) {
   addParam += "&paramTitle=Reports";
   gotoscrn("scrnReports",addParam);
}

function genReport(parentId) {
   var url = getRPTCriteria(parentId);
   url = "spmsReport.php?" + url;
   $("#rptHolder").html("");
   $("#rptHolder").load(url, function(responseTxt, statusTxt, xhr){
      if(statusTxt == "error")
         alert("Ooops Error: " + xhr.status + " : " + xhr.statusText);
         return false;
   });
}

function getRPTCriteria(parentId){
   var Elem_Value = "";
   var Elem_Name = "";
   var Elem_Type = "";
   var objvalue = "";
   var ObjClassName = "";
   var isInclude = false;
   $("#"+parentId+" .rptCriteria--").each(function()
   {
      ObjClassName = $(this).attr("class");
      Elem_Value = $(this).val();
      Elem_Name  = $(this).attr("name");
      isInclude = false;
      if ($(this).is("select")) {
         Elem_Type  = "select";
      } else if ($(this).is("textarea")) {
         Elem_Type  = "textarea";
      } else {
         Elem_Type  = $(this).attr("type");
      }
      if (Elem_Value != "") {
         objvalue += Elem_Name+"="+Elem_Value+"&";
      }
   });

   $("#"+parentId+" .showCol--").each(function()
   {
      ObjClassName = $(this).attr("class");
      Elem_Value = $(this).is(":checked");
      Elem_Name  = $(this).attr("name");
      isInclude = false;

      if ($(this).is("select")) {
         Elem_Type  = "select";
      } else if ($(this).is("textarea")) {
         Elem_Type  = "textarea";
      } else {
         Elem_Type  = $(this).attr("type");
      }

      if (Elem_Value == true) {
         objvalue += Elem_Name+"="+Elem_Value+"&";
      }
   });
   return objvalue;
}
