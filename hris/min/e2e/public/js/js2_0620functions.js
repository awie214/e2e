$(document).ready(function(){
   $("#btnSearchEmployees").click(function () {
      //if ($("[name='SearchEmployees']").val() == "") return;
      //$("#RecordDetails").html("");
      //$("#btnHolder").hide();
      $.get("trn.e2e.php",
      {
         fn:"searchEmployees",
         hCtrl:$("#hController").val(),
         srch:$("[name='SearchEmployees']").val(),
         hCompanyID:$("#hCompanyID").val(),
         hBranchID:$("#hBranchID").val(),
         hEmpRefId:$("#hEmpRefId").val(),
         hUserRefId:$("#hUserRefId").val(),
         Inactive:$("#Inactive").val(),
         table:$("#filter_table").val(),
         refid:$("#filter_value").val()

      },
      function(data,status) {
         if (status == "success") {
            try {
               $("#divEmployeesList").html(data);
               $("[name='SearchEmployees']").select();
               $("[name='SearchEmployees']").focus();

            } catch (e) {
                if (e instanceof SyntaxError) {
                    alert(e.message);
                }
            }
         }
      });
   });
   $(".topMenu, .subMenu, .Menu").each(function () {
      $(this).click(function () {
         $("#hmemof").val($(this).attr("memof"));
         if ($(this).attr("id") != undefined) {
            setMenuClick($(this).attr("id"));
         } 
         if (
            $(this).attr("pre") != undefined &&
            $(this).attr("route") != undefined
         ) {
            var addParam = "&id=" + $(this).attr("id");
            addParam += "&paramTitle=" + $(this).attr("titleBar");
            //alert($(this).attr("pre") + $(this).attr("route"));
            gotoscrn($(this).attr("pre") + $(this).attr("route"), addParam);
         }
      });
   });
   $("#btnRESETPW").click(function() {
      if ($("#tSecret").val() != "" && $("#hUserRefID").val() > 0) {
         var secret = hash($("#tSecret").val());
         $("#tSecret").val("");
         $.get("trn.e2e.php",
            {
               fn: "verifyPW",
               u: $("#hUserRefID").val(),
               s: secret
            },
            function (data, status) {
               if (status == "error") return false;
               if (status == "success") {
                  try {
                     eval(data);   
                  }
                  catch (e) {
                     if (e instanceof SyntaxError) {
                        alert(e.message);
                     }
                  }
               }
            });
      } else {
         $.notify("Blank Password or No User Id ... ");
         return false;
      }
   }); 
});

function closePop() {
   setBHV("innerHTML","&nbsp;","idPopOut");
   setBHV("styleDisplay","none","idPopOut");
   setBHV("styleDisplay","none","idBlack");
}
function gotoscrn(file,param){
   $("#hProg").val(file);
   if (param!=="") {
      var elem = param.split("&");
      var addElem = "";
      for (i=0;i<elem.length;i++) {
         if (elem[i] !== "") {
            addElem += '<input type="hidden" value="'+elem[i].split("=")[1]+'" name="'+elem[i].split("=")[0]+'">\n';
         }
      }
      $("#hEmpRefId").append(addElem);

   }
   var url = "";
   url += "GlobalCaller.e2e.php?";
   url += $("#hgParam").val();
   url += "&file=" + file;
   url += param;
   if ($("#hgParam").val() == undefined && $("#hgParam").val() == "") {
      console.log("System Error hgParam undefined");
      $.post("SystemAjax.e2e.php",
      {
         task:"writesyslog",
         file:file,
         param:param
      },
      function (data, status) {
         if (status == "success") {
            code = data;
            try {
               eval(code);
            } catch (e) {
               if (e instanceof SyntaxError) {
                  alert(e.message);
               }
            }
         }
         else {
            alert("Ooops Error : " + status + "[005]");
         }
      });
   } else {
      $.get("trn.e2e.php",
      {
         fn: "NewPageSession",
         param: param
      },
      function (data, status) {
         if (status == "success") {
            code = data;
            try {
               var d = new Date();
               var n = $("#hUser").val() + "_" + data;
               setSession("AUTH_PAGE", n);
               self.location = url + "&auth=" + n;
            } catch (e) {
               if (e instanceof SyntaxError) {
                  alert(e.message);
               }
            }
         }
         else {
            alert("Ooops Error : " + status + "[012]");
         }
      });
   }


   //$("[name='xForm']").submit();

   /*$.ajax({
      url: "GlobalCaller.e2e.php?",
      type: "POST",
      data: new FormData($("[name='xForm']")[0]),
      success : function(responseTxt){
      },
      enctype: 'multipart/form-data',
      processData: false,
      contentType: false,
      cache: false
   });*/
}

function gotosubscrn(file,idx) {
   var url = "";
   url  = file + ".e2e.php?idx="+idx;
   url += "&hSess="+$("[name='hSess']").val();
   url += "&hUser="+$("[name='hUser']").val();
   url += "&hUserRefId="+$("[name='hUserRefID']").val();
   url += "&hCompanyID="+$("[name='hCompanyID']").val();
   url += "&hBranchID="+$("[name='hBranchID']").val();
   $("#overlay").empty();
   $("#overlay").load(url,callbackfunction());
}
function resetUserPW(empRefId) {
   //alert(empRefId);
   $.post("trnPW.e2e.php",
   {
      task:"reset pw",
      empRefId:empRefId
   },
   function (data, status) {
      if (status == "success") {
         code = data;
         try {
            alert(code);
         } catch (e) {
            if (e instanceof SyntaxError) {
               alert(e.message);
            }
         }
      }
      else {
         alert("Ooops Error : " + status + "[005]");
      }
   });
}

function upload(id) {
   var myPopUp = null;
   var url     = "";
   var popname = 'myPopUp';
   var left = (screen.width/2)-(400/2);
   var top = (screen.height/2)-(400/2);
   var attr    = 'toolbar=no,scrollbars=no,resizable=no,top='+top+',left='+left+',width=400,height=400';
   var replace = 'replace=yes';
   url = "upload.e2e.php?id=" + id;
   myPopUp = window.open(url,popname,attr,replace);
   myPopUp.focus();
}

function changePW(user) {
   var myPopUp = null;
   var url     = "";
   var popname = 'myPopUp';
   var left = (screen.width/2)-(400/2);
   var top = (screen.height/2)-(400/2);
   var attr    = 'toolbar=no,scrollbars=no,resizable=no,top='+top+',left='+left+',width=400,height=400';
   var replace = 'replace=yes';
   url = "chPW.e2e.php?u="+user;
   myPopUp = window.open(url,popname,attr,replace);
   myPopUp.focus();
}

function remIconDL() {
   $(".newDataLibrary").hide();
   $(".createSelect--").css("width","100%");
}

function hash(objval) {
   try {
      var hash_Variant = "SHA-1";
      var hash_InputType = "TEXT";
      var hash_Rounds = "3";

      var hashObj = new jsSHA(
         hash_Variant,
         hash_InputType,
         {numRounds: parseInt(hash_Rounds, 10)}
      );
      hashObj.update(objval);
      return hashObj.getHash("HEX");
   } catch(e) {
      return e.message;
   }
}

function openNav_adm() {
    document.getElementById("mySidenav").style.width = "250px";
}
function closeNav_adm() {
    document.getElementById("mySidenav").style.width = "0";
}
function openNav_AgencyConfig() {
    document.getElementById("myAgencyConfig").style.width = "250px";
}
function closeNav_AgencyConfig() {
    document.getElementById("myAgencyConfig").style.width = "0";
}